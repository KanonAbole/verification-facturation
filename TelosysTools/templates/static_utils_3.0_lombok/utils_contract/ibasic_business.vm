
/*
 * Created on $today.date ( Time $today.time )
 * Generator tool : $generator.name ( version $generator.version )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ${target.javaPackageFromFolder(${SRC})};

import java.util.Locale;
import java.text.ParseException;

import org.springframework.dao.DataAccessException;

/**
 * IBasic Business
 * 
 * @author Geo
 *
 */
public interface IBasicBusiness<T, K> {

	/**
	 * create Object by using T as object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	public abstract K create(T request, Locale locale) throws ParseException, Exception;

	/**
	 * update Object by using T as object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	public abstract K update(T request, Locale locale) throws ParseException, Exception;

	/**
	 * delete Object by using T as object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	public abstract K delete(T request, Locale locale) throws ParseException, Exception;

	/**
	 * get a List of Object by using T as criteria object.
	 * 
	 * @param T
	 * @return K
	 * 
	 */
	public abstract K getByCriteria(T request, Locale locale) throws ParseException, Exception, DataAccessException;
}
