
/*
 * Java dto for entity table facture 
 * Created on 2023-02-08 ( Time 15:51:20 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.*;

import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.dto.customize._FactureDto;

/**
 * DTO for table "facture"
 *
 * @author Geo
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class FactureDto extends _FactureDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private Integer    numFacture           ;
	private String     dateSoin             ;
    private Integer    numBon               ;
    private Integer    montantTtc           ;
    private Integer    tauxPcFacture        ;
    private Integer    montantExclu         ;
    private Integer    montantCharge        ;
	private String     createdAt            ;
    private Integer    partPatient          ;
    private String     codeAffection        ;
    private Integer    numTransmission      ;
    private Integer    partAssurance        ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private String     codeMedPrescripteur  ;
    private Boolean    estTraiter           ;
	private String     updatedAt            ;
    private Boolean    estPayer             ;
    private Boolean    isDeleted            ;
    private Integer    idAdherent           ;
    private Integer    idNatureActe         ;
    private Integer    idPrestataire        ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String natureActeLibelle;
	private String adherentNom;
	private String adherentPrenom;
	private String prestataireNom;
	private String prestatairePrenom;

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  numFactureParam       ;                     
	private SearchParam<String>   dateSoinParam         ;                     
	private SearchParam<Integer>  numBonParam           ;                     
	private SearchParam<Integer>  montantTtcParam       ;                     
	private SearchParam<Integer>  tauxPcFactureParam    ;                     
	private SearchParam<Integer>  montantExcluParam     ;                     
	private SearchParam<Integer>  montantChargeParam    ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  partPatientParam      ;                     
	private SearchParam<String>   codeAffectionParam    ;                     
	private SearchParam<Integer>  numTransmissionParam  ;                     
	private SearchParam<Integer>  partAssuranceParam    ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<String>   codeMedPrescripteurParam;                     
	private SearchParam<Boolean>  estTraiterParam       ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Boolean>  estPayerParam         ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  idAdherentParam       ;                     
	private SearchParam<Integer>  idNatureActeParam     ;                     
	private SearchParam<Integer>  idPrestataireParam    ;                     
	private SearchParam<String>   natureActeLibelleParam;                     
	private SearchParam<String>   adherentNomParam      ;                     
	private SearchParam<String>   adherentPrenomParam   ;                     
	private SearchParam<String>   prestataireNomParam   ;                     
	private SearchParam<String>   prestatairePrenomParam;                     
    /**
     * Default constructor
     */
    public FactureDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
