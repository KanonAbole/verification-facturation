

/*
 * Java transformer for entity table nature_acte 
 * Created on 2023-02-08 ( Time 10:54:48 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.dao.entity.*;


/**
 * TRANSFORMER for table "nature_acte"
 * 
 * @author Geo
 *
 */
@Mapper
public interface NatureActeTransformer {

	NatureActeTransformer INSTANCE = Mappers.getMapper(NatureActeTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
	})
	NatureActeDto toDto(NatureActe entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<NatureActeDto> toDtos(List<NatureActe> entities) throws ParseException;

    default NatureActeDto toLiteDto(NatureActe entity) {
		if (entity == null) {
			return null;
		}
		NatureActeDto dto = new NatureActeDto();
		dto.setId( entity.getId() );
		dto.setLibelle( entity.getLibelle() );
		return dto;
    }

	default List<NatureActeDto> toLiteDtos(List<NatureActe> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<NatureActeDto> dtos = new ArrayList<NatureActeDto>();
		for (NatureActe entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    NatureActe toEntity(NatureActeDto dto) throws ParseException;

    //List<NatureActe> toEntities(List<NatureActeDto> dtos) throws ParseException;

}
