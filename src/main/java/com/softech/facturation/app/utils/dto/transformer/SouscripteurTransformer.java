

/*
 * Java transformer for entity table souscripteur 
 * Created on 2023-02-08 ( Time 14:31:44 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.dao.entity.*;


/**
 * TRANSFORMER for table "souscripteur"
 * 
 * @author Geo
 *
 */
@Mapper
public interface SouscripteurTransformer {

	SouscripteurTransformer INSTANCE = Mappers.getMapper(SouscripteurTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
	})
	SouscripteurDto toDto(Souscripteur entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<SouscripteurDto> toDtos(List<Souscripteur> entities) throws ParseException;

    default SouscripteurDto toLiteDto(Souscripteur entity) {
		if (entity == null) {
			return null;
		}
		SouscripteurDto dto = new SouscripteurDto();
		dto.setId( entity.getId() );
		dto.setLibelle( entity.getLibelle() );
		return dto;
    }

	default List<SouscripteurDto> toLiteDtos(List<Souscripteur> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<SouscripteurDto> dtos = new ArrayList<SouscripteurDto>();
		for (Souscripteur entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    Souscripteur toEntity(SouscripteurDto dto) throws ParseException;

    //List<Souscripteur> toEntities(List<SouscripteurDto> dtos) throws ParseException;

}
