

/*
 * Java transformer for entity table prestataire 
 * Created on 2023-02-08 ( Time 10:54:49 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.dao.entity.*;


/**
 * TRANSFORMER for table "prestataire"
 * 
 * @author Geo
 *
 */
@Mapper
public interface PrestataireTransformer {

	PrestataireTransformer INSTANCE = Mappers.getMapper(PrestataireTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
	})
	PrestataireDto toDto(Prestataire entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<PrestataireDto> toDtos(List<Prestataire> entities) throws ParseException;

    default PrestataireDto toLiteDto(Prestataire entity) {
		if (entity == null) {
			return null;
		}
		PrestataireDto dto = new PrestataireDto();
		dto.setId( entity.getId() );
		dto.setNom( entity.getNom() );
		dto.setPrenom( entity.getPrenom() );
		return dto;
    }

	default List<PrestataireDto> toLiteDtos(List<Prestataire> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<PrestataireDto> dtos = new ArrayList<PrestataireDto>();
		for (Prestataire entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.codePrestataire", target="codePrestataire"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.prenom", target="prenom"),
		@Mapping(source="dto.contact", target="contact"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    Prestataire toEntity(PrestataireDto dto) throws ParseException;

    //List<Prestataire> toEntities(List<PrestataireDto> dtos) throws ParseException;

}
