

/*
 * Java transformer for entity table adherent 
 * Created on 2023-02-08 ( Time 14:31:41 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.dao.entity.*;


/**
 * TRANSFORMER for table "adherent"
 * 
 * @author Geo
 *
 */
@Mapper
public interface AdherentTransformer {

	AdherentTransformer INSTANCE = Mappers.getMapper(AdherentTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.dateNaissance", dateFormat="dd/MM/yyyy",target="dateNaissance"),
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.souscripteur.id", target="idSouscripteur"),
		@Mapping(source="entity.souscripteur.libelle", target="souscripteurLibelle"),
	})
	AdherentDto toDto(Adherent entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<AdherentDto> toDtos(List<Adherent> entities) throws ParseException;

    default AdherentDto toLiteDto(Adherent entity) {
		if (entity == null) {
			return null;
		}
		AdherentDto dto = new AdherentDto();
		dto.setId( entity.getId() );
		dto.setNom( entity.getNom() );
		dto.setPrenom( entity.getPrenom() );
		return dto;
    }

	default List<AdherentDto> toLiteDtos(List<Adherent> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<AdherentDto> dtos = new ArrayList<AdherentDto>();
		for (Adherent entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.matricule", target="matricule"),
		@Mapping(source="dto.numAdherent", target="numAdherent"),
		@Mapping(source="dto.tauxPriseCharge", target="tauxPriseCharge"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.prenom", target="prenom"),
		@Mapping(source="dto.dateNaissance", dateFormat="dd/MM/yyyy",target="dateNaissance"),
		@Mapping(source="dto.lieuNaissance", target="lieuNaissance"),
		@Mapping(source="dto.agent", target="agent"),
		@Mapping(source="dto.fonction", target="fonction"),
		@Mapping(source="dto.telephone1", target="telephone1"),
		@Mapping(source="dto.telephone2", target="telephone2"),
		@Mapping(source="dto.sexe", target="sexe"),
		@Mapping(source="dto.photo", target="photo"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="souscripteur", target="souscripteur"),
	})
    Adherent toEntity(AdherentDto dto, Souscripteur souscripteur) throws ParseException;

    //List<Adherent> toEntities(List<AdherentDto> dtos) throws ParseException;

}
