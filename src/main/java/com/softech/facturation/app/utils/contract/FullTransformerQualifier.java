/*
 * Created on 2023-02-08 ( Time 09:09:50 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.contract;


import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import org.mapstruct.Qualifier;

/**
 * Request Base
 * 
 * @author Geo
 *
 */
@Qualifier
@Target(ElementType.METHOD)
public @interface FullTransformerQualifier {

}
