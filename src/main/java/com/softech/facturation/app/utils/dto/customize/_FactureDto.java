
/*
 * Java dto for entity table facture 
 * Created on 2023-02-08 ( Time 09:10:23 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.dto.customize;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.softech.facturation.app.utils.contract.*;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO customize for table "facture"
 * 
 * @author Geo
 *
 */
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class _FactureDto {
	private Integer numReglement;
	private String libelleActe;
	private String matriculeAdherent;
	private String CodePrestataire;
}
