

/*
 * Java transformer for entity table reglement 
 * Created on 2023-02-13 ( Time 13:07:55 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.dao.entity.*;


/**
 * TRANSFORMER for table "reglement"
 * 
 * @author Geo
 *
 */
@Mapper
public interface ReglementTransformer {

	ReglementTransformer INSTANCE = Mappers.getMapper(ReglementTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.dateReglement", dateFormat="dd/MM/yyyy",target="dateReglement"),
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.facture.id", target="idFacture"),
	})
	ReglementDto toDto(Reglement entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<ReglementDto> toDtos(List<Reglement> entities) throws ParseException;

    default ReglementDto toLiteDto(Reglement entity) {
		if (entity == null) {
			return null;
		}
		ReglementDto dto = new ReglementDto();
		dto.setId( entity.getId() );
		return dto;
    }

	default List<ReglementDto> toLiteDtos(List<Reglement> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<ReglementDto> dtos = new ArrayList<ReglementDto>();
		for (Reglement entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.numReglement", target="numReglement"),
		@Mapping(source="dto.dateReglement", dateFormat="dd/MM/yyyy",target="dateReglement"),
		@Mapping(source="dto.montantRegle", target="montantRegle"),
		@Mapping(source="dto.montantLettre", target="montantLettre"),
		@Mapping(source="dto.modeReglement", target="modeReglement"),
		@Mapping(source="dto.numCheque", target="numCheque"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="facture", target="facture"),
	})
    Reglement toEntity(ReglementDto dto, Facture facture) throws ParseException;

    //List<Reglement> toEntities(List<ReglementDto> dtos) throws ParseException;

}
