
/*
 * Java dto for entity table adherent 
 * Created on 2023-02-08 ( Time 14:31:41 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.*;

import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.dto.customize._AdherentDto;

/**
 * DTO for table "adherent"
 *
 * @author Geo
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class AdherentDto extends _AdherentDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private String     matricule            ;
    private String     numAdherent          ;
    private Integer    tauxPriseCharge      ;
    private String     nom                  ;
    private String     prenom               ;
	private String     dateNaissance        ;
    private String     lieuNaissance        ;
    private Boolean    agent                ;
    private String     fonction             ;
    private String     telephone1           ;
    private String     telephone2           ;
    private String     sexe                 ;
    private String     photo                ;
	private String     createdAt            ;
	private String     updatedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Boolean    isDeleted            ;
    private Integer    idSouscripteur       ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String souscripteurLibelle;

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<String>   matriculeParam        ;                     
	private SearchParam<String>   numAdherentParam      ;                     
	private SearchParam<Integer>  tauxPriseChargeParam  ;                     
	private SearchParam<String>   nomParam              ;                     
	private SearchParam<String>   prenomParam           ;                     
	private SearchParam<String>   dateNaissanceParam    ;                     
	private SearchParam<String>   lieuNaissanceParam    ;                     
	private SearchParam<Boolean>  agentParam            ;                     
	private SearchParam<String>   fonctionParam         ;                     
	private SearchParam<String>   telephone1Param       ;                     
	private SearchParam<String>   telephone2Param       ;                     
	private SearchParam<String>   sexeParam             ;                     
	private SearchParam<String>   photoParam            ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  idSouscripteurParam   ;                     
	private SearchParam<String>   souscripteurLibelleParam;                     
    /**
     * Default constructor
     */
    public AdherentDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
