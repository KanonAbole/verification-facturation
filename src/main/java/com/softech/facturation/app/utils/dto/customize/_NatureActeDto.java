
/*
 * Java dto for entity table nature_acte 
 * Created on 2023-02-08 ( Time 09:10:23 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.dto.customize;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.softech.facturation.app.utils.contract.*;

/**
 * DTO customize for table "nature_acte"
 * 
 * @author Geo
 *
 */
@JsonInclude(Include.NON_NULL)
public class _NatureActeDto {

}
