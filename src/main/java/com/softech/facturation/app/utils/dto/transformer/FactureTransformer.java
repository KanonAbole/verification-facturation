

/*
 * Java transformer for entity table facture 
 * Created on 2023-02-08 ( Time 15:51:20 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.dao.entity.*;


/**
 * TRANSFORMER for table "facture"
 * 
 * @author Geo
 *
 */
@Mapper
public interface FactureTransformer {

	FactureTransformer INSTANCE = Mappers.getMapper(FactureTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.dateSoin", dateFormat="dd/MM/yyyy",target="dateSoin"),
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.natureActe.id", target="idNatureActe"),
		@Mapping(source="entity.natureActe.libelle", target="natureActeLibelle"),
		@Mapping(source="entity.adherent.id", target="idAdherent"),
		@Mapping(source="entity.adherent.nom", target="adherentNom"),
		@Mapping(source="entity.adherent.prenom", target="adherentPrenom"),
		@Mapping(source="entity.prestataire.id", target="idPrestataire"),
		@Mapping(source="entity.prestataire.nom", target="prestataireNom"),
		@Mapping(source="entity.prestataire.prenom", target="prestatairePrenom"),
	})
	FactureDto toDto(Facture entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<FactureDto> toDtos(List<Facture> entities) throws ParseException;

    default FactureDto toLiteDto(Facture entity) {
		if (entity == null) {
			return null;
		}
		FactureDto dto = new FactureDto();
		dto.setId( entity.getId() );
		return dto;
    }

	default List<FactureDto> toLiteDtos(List<Facture> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<FactureDto> dtos = new ArrayList<FactureDto>();
		for (Facture entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.numFacture", target="numFacture"),
		@Mapping(source="dto.dateSoin", dateFormat="dd/MM/yyyy",target="dateSoin"),
		@Mapping(source="dto.numBon", target="numBon"),
		@Mapping(source="dto.montantTtc", target="montantTtc"),
		@Mapping(source="dto.tauxPcFacture", target="tauxPcFacture"),
		@Mapping(source="dto.montantExclu", target="montantExclu"),
		@Mapping(source="dto.montantCharge", target="montantCharge"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.partPatient", target="partPatient"),
		@Mapping(source="dto.codeAffection", target="codeAffection"),
		@Mapping(source="dto.numTransmission", target="numTransmission"),
		@Mapping(source="dto.partAssurance", target="partAssurance"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.codeMedPrescripteur", target="codeMedPrescripteur"),
		@Mapping(source="dto.estTraiter", target="estTraiter"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.estPayer", target="estPayer"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="natureActe", target="natureActe"),
		@Mapping(source="adherent", target="adherent"),
		@Mapping(source="prestataire", target="prestataire"),
	})
    Facture toEntity(FactureDto dto, NatureActe natureActe, Adherent adherent, Prestataire prestataire) throws ParseException;

    //List<Facture> toEntities(List<FactureDto> dtos) throws ParseException;

}
