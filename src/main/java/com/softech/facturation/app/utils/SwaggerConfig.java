/*
* Created on 2023-02-08 ( Time 09:09:49 )
* Generator tool : Telosys Tools Generator ( version 3.3.0 )
* Copyright 2018 Geo. All Rights Reserved.
*/

package com.softech.facturation.app.utils;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.StringVendorExtension;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Geo
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                // .apis(RequestHandlerSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.softech.facturation.app.rest"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

	private ApiInfo apiInfo() {
	     Collection<VendorExtension> vendorExtensions = new ArrayList<>();
	     vendorExtensions.add( new StringVendorExtension("owner", "P. TECH"));
	     vendorExtensions.add( new StringVendorExtension("development_team", "P. Backend"));

	     Contact contactInfo = new Contact("P Tech", "http://ptech.ci",
	            "tsegbas@gmail.com");
	     return new ApiInfo(
	         "API",
	         "API REST",
	         "1.0",
	         "",
	         contactInfo,
	         "OpenSource - Apache 2.0",
	         "http://www.apache.org",
	         vendorExtensions);
	   }
}