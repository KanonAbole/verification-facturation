
/*
 * Java dto for entity table reglement 
 * Created on 2023-02-13 ( Time 13:07:55 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.utils.dto;

import java.util.Date;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.*;

import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.dto.customize._ReglementDto;

/**
 * DTO for table "reglement"
 *
 * @author Geo
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class ReglementDto extends _ReglementDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private Integer    numReglement         ;
	private String     dateReglement        ;
    private Integer    montantRegle         ;
    private String     montantLettre        ;
    private String     modeReglement        ;
    private Integer    numCheque            ;
	private String     createdAt            ;
    private Integer    createdBy            ;
	private String     updatedAt            ;
    private Integer    updatedBy            ;
    private Boolean    isDeleted            ;
    private Integer    idFacture            ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  numReglementParam     ;                     
	private SearchParam<String>   dateReglementParam    ;                     
	private SearchParam<Integer>  montantRegleParam     ;                     
	private SearchParam<String>   montantLettreParam    ;                     
	private SearchParam<String>   modeReglementParam    ;                     
	private SearchParam<Integer>  numChequeParam        ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<Integer>  idFactureParam        ;                     
    /**
     * Default constructor
     */
    public ReglementDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
