package com.softech.facturation.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FacturationAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(FacturationAppApplication.class, args);
	}

}
