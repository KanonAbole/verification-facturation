/*
 * Created on 2023-02-08 ( Time 09:09:50 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Younous. All Rights Reserved.
 */

package com.softech.facturation.app.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.jboss.logging.Logger;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * LogAspect
 * 
 * @author Younous
 *
 */

@Component
@Aspect
@EnableAspectJAutoProxy
public class LogAspect {

	Logger logger = Logger.getLogger(LogAspect.class.getName());

	@Around("execution(* com.softech.facturation.app.rest.api..*(..))")
	public Object logController(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

		logger.info("start method " + proceedingJoinPoint.getSignature());

		Object result = proceedingJoinPoint.proceed();

		logger.info("end method " + proceedingJoinPoint.getSignature());

		return result;
	}

	@Around("execution(* com.softech.facturation.app.business..*(..))")
	public Object logBusiness(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

		logger.info("begin " + proceedingJoinPoint.getSignature());

		Object result = proceedingJoinPoint.proceed();

		logger.info("end " + proceedingJoinPoint.getSignature());

		return result;
	}
}
