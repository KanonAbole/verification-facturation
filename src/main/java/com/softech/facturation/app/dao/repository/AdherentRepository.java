package com.softech.facturation.app.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.softech.facturation.app.utils.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.contract.Request;
import com.softech.facturation.app.utils.contract.Response;
import com.softech.facturation.app.dao.entity.*;
import com.softech.facturation.app.dao.repository.customize._AdherentRepository;

/**
 * Repository : Adherent.
 */
@Repository
public interface AdherentRepository extends JpaRepository<Adherent, Integer>, _AdherentRepository {
	/**
	 * Finds Adherent by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Adherent whose id is equals to the given id. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.id = :id and e.isDeleted = :isDeleted")
	Adherent findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Adherent by using matricule as a search criteria.
	 * 
	 * @param matricule
	 * @return An Object Adherent whose matricule is equals to the given matricule. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.matricule = :matricule and e.isDeleted = :isDeleted")
	Adherent findByMatricule(@Param("matricule")String matricule, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using numAdherent as a search criteria.
	 * 
	 * @param numAdherent
	 * @return An Object Adherent whose numAdherent is equals to the given numAdherent. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.numAdherent = :numAdherent and e.isDeleted = :isDeleted")
	List<Adherent> findByNumAdherent(@Param("numAdherent")String numAdherent, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using tauxPriseCharge as a search criteria.
	 * 
	 * @param tauxPriseCharge
	 * @return An Object Adherent whose tauxPriseCharge is equals to the given tauxPriseCharge. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.tauxPriseCharge = :tauxPriseCharge and e.isDeleted = :isDeleted")
	List<Adherent> findByTauxPriseCharge(@Param("tauxPriseCharge")Integer tauxPriseCharge, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using nom as a search criteria.
	 * 
	 * @param nom
	 * @return An Object Adherent whose nom is equals to the given nom. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.nom = :nom and e.isDeleted = :isDeleted")
	List<Adherent> findByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using prenom as a search criteria.
	 * 
	 * @param prenom
	 * @return An Object Adherent whose prenom is equals to the given prenom. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.prenom = :prenom and e.isDeleted = :isDeleted")
	List<Adherent> findByPrenom(@Param("prenom")String prenom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using dateNaissance as a search criteria.
	 * 
	 * @param dateNaissance
	 * @return An Object Adherent whose dateNaissance is equals to the given dateNaissance. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.dateNaissance = :dateNaissance and e.isDeleted = :isDeleted")
	List<Adherent> findByDateNaissance(@Param("dateNaissance")Date dateNaissance, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using lieuNaissance as a search criteria.
	 * 
	 * @param lieuNaissance
	 * @return An Object Adherent whose lieuNaissance is equals to the given lieuNaissance. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.lieuNaissance = :lieuNaissance and e.isDeleted = :isDeleted")
	List<Adherent> findByLieuNaissance(@Param("lieuNaissance")String lieuNaissance, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using agent as a search criteria.
	 * 
	 * @param agent
	 * @return An Object Adherent whose agent is equals to the given agent. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.agent = :agent and e.isDeleted = :isDeleted")
	List<Adherent> findByAgent(@Param("agent")Boolean agent, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using fonction as a search criteria.
	 * 
	 * @param fonction
	 * @return An Object Adherent whose fonction is equals to the given fonction. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.fonction = :fonction and e.isDeleted = :isDeleted")
	List<Adherent> findByFonction(@Param("fonction")String fonction, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using telephone1 as a search criteria.
	 * 
	 * @param telephone1
	 * @return An Object Adherent whose telephone1 is equals to the given telephone1. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.telephone1 = :telephone1 and e.isDeleted = :isDeleted")
	List<Adherent> findByTelephone1(@Param("telephone1")String telephone1, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using telephone2 as a search criteria.
	 * 
	 * @param telephone2
	 * @return An Object Adherent whose telephone2 is equals to the given telephone2. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.telephone2 = :telephone2 and e.isDeleted = :isDeleted")
	List<Adherent> findByTelephone2(@Param("telephone2")String telephone2, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using sexe as a search criteria.
	 * 
	 * @param sexe
	 * @return An Object Adherent whose sexe is equals to the given sexe. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.sexe = :sexe and e.isDeleted = :isDeleted")
	List<Adherent> findBySexe(@Param("sexe")String sexe, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using photo as a search criteria.
	 * 
	 * @param photo
	 * @return An Object Adherent whose photo is equals to the given photo. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.photo = :photo and e.isDeleted = :isDeleted")
	List<Adherent> findByPhoto(@Param("photo")String photo, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Adherent whose createdAt is equals to the given createdAt. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Adherent> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Adherent whose updatedAt is equals to the given updatedAt. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Adherent> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Adherent whose createdBy is equals to the given createdBy. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Adherent> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Adherent whose updatedBy is equals to the given updatedBy. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Adherent> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Adherent by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Adherent whose isDeleted is equals to the given isDeleted. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.isDeleted = :isDeleted")
	List<Adherent> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Adherent by using idSouscripteur as a search criteria.
	 * 
	 * @param idSouscripteur
	 * @return An Object Adherent whose idSouscripteur is equals to the given idSouscripteur. If
	 *         no Adherent is found, this method returns null.
	 */
	@Query("select e from Adherent e where e.souscripteur.id = :idSouscripteur and e.isDeleted = :isDeleted")
	List<Adherent> findByIdSouscripteur(@Param("idSouscripteur")Integer idSouscripteur, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of Adherent by using adherentDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Adherent
	 * @throws DataAccessException,ParseException
	 */
	default List<Adherent> getByCriteria(Request<AdherentDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Adherent e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Adherent> query = em.createQuery(req, Adherent.class);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Adherent by using adherentDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Adherent
	 * 
	 */
	default Long count(Request<AdherentDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Adherent e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<AdherentDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		AdherentDto dto = request.getData() != null ? request.getData() : new AdherentDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (AdherentDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(AdherentDto dto, HashMap<String, java.lang.Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMatricule())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("matricule", dto.getMatricule(), "e.matricule", "String", dto.getMatriculeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNumAdherent())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numAdherent", dto.getNumAdherent(), "e.numAdherent", "String", dto.getNumAdherentParam(), param, index, locale));
			}
			if (dto.getTauxPriseCharge()!= null && dto.getTauxPriseCharge() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("tauxPriseCharge", dto.getTauxPriseCharge(), "e.tauxPriseCharge", "Integer", dto.getTauxPriseChargeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nom", dto.getNom(), "e.nom", "String", dto.getNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prenom", dto.getPrenom(), "e.prenom", "String", dto.getPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateNaissance())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateNaissance", dto.getDateNaissance(), "e.dateNaissance", "Date", dto.getDateNaissanceParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLieuNaissance())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("lieuNaissance", dto.getLieuNaissance(), "e.lieuNaissance", "String", dto.getLieuNaissanceParam(), param, index, locale));
			}
			if (dto.getAgent()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("agent", dto.getAgent(), "e.agent", "Boolean", dto.getAgentParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getFonction())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("fonction", dto.getFonction(), "e.fonction", "String", dto.getFonctionParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephone1())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephone1", dto.getTelephone1(), "e.telephone1", "String", dto.getTelephone1Param(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getTelephone2())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("telephone2", dto.getTelephone2(), "e.telephone2", "String", dto.getTelephone2Param(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSexe())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("sexe", dto.getSexe(), "e.sexe", "String", dto.getSexeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPhoto())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("photo", dto.getPhoto(), "e.photo", "String", dto.getPhotoParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getIdSouscripteur()!= null && dto.getIdSouscripteur() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idSouscripteur", dto.getIdSouscripteur(), "e.souscripteur.id", "Integer", dto.getIdSouscripteurParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSouscripteurLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("souscripteurLibelle", dto.getSouscripteurLibelle(), "e.souscripteur.libelle", "String", dto.getSouscripteurLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
