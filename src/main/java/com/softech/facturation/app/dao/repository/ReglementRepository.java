package com.softech.facturation.app.dao.repository;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.softech.facturation.app.utils.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.contract.Request;
import com.softech.facturation.app.utils.contract.Response;
import com.softech.facturation.app.dao.entity.*;
import com.softech.facturation.app.dao.repository.customize._ReglementRepository;

/**
 * Repository : Reglement.
 */
@Repository
public interface ReglementRepository extends JpaRepository<Reglement, Integer>, _ReglementRepository {
	/**
	 * Finds Reglement by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Reglement whose id is equals to the given id. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.id = :id and e.isDeleted = :isDeleted")
	Reglement findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Reglement by using numReglement as a search criteria.
	 * 
	 * @param numReglement
	 * @return An Object Reglement whose numReglement is equals to the given numReglement. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.numReglement = :numReglement and e.isDeleted = :isDeleted")
	List<Reglement> findByNumReglement(@Param("numReglement")Integer numReglement, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Reglement by using dateReglement as a search criteria.
	 * 
	 * @param dateReglement
	 * @return An Object Reglement whose dateReglement is equals to the given dateReglement. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.dateReglement = :dateReglement and e.isDeleted = :isDeleted")
	List<Reglement> findByDateReglement(@Param("dateReglement")Date dateReglement, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Reglement by using montantRegle as a search criteria.
	 * 
	 * @param montantRegle
	 * @return An Object Reglement whose montantRegle is equals to the given montantRegle. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.montantRegle = :montantRegle and e.isDeleted = :isDeleted")
	List<Reglement> findByMontantRegle(@Param("montantRegle")Integer montantRegle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Reglement by using montantLettre as a search criteria.
	 * 
	 * @param montantLettre
	 * @return An Object Reglement whose montantLettre is equals to the given montantLettre. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.montantLettre = :montantLettre and e.isDeleted = :isDeleted")
	List<Reglement> findByMontantLettre(@Param("montantLettre")String montantLettre, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Reglement by using modeReglement as a search criteria.
	 * 
	 * @param modeReglement
	 * @return An Object Reglement whose modeReglement is equals to the given modeReglement. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.modeReglement = :modeReglement and e.isDeleted = :isDeleted")
	List<Reglement> findByModeReglement(@Param("modeReglement")String modeReglement, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Reglement by using numCheque as a search criteria.
	 * 
	 * @param numCheque
	 * @return An Object Reglement whose numCheque is equals to the given numCheque. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.numCheque = :numCheque and e.isDeleted = :isDeleted")
	List<Reglement> findByNumCheque(@Param("numCheque")Integer numCheque, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Reglement by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Reglement whose createdAt is equals to the given createdAt. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Reglement> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Reglement by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Reglement whose createdBy is equals to the given createdBy. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Reglement> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Reglement by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Reglement whose updatedAt is equals to the given updatedAt. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Reglement> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Reglement by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Reglement whose updatedBy is equals to the given updatedBy. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Reglement> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Reglement by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Reglement whose isDeleted is equals to the given isDeleted. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.isDeleted = :isDeleted")
	List<Reglement> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Reglement by using idFacture as a search criteria.
	 * 
	 * @param idFacture
	 * @return An Object Reglement whose idFacture is equals to the given idFacture. If
	 *         no Reglement is found, this method returns null.
	 */
	@Query("select e from Reglement e where e.facture.id = :idFacture and e.isDeleted = :isDeleted")
	List<Reglement> findByIdFacture(@Param("idFacture")Integer idFacture, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of Reglement by using reglementDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Reglement
	 * @throws DataAccessException,ParseException
	 */
	default List<Reglement> getByCriteria(Request<ReglementDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Reglement e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Reglement> query = em.createQuery(req, Reglement.class);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Reglement by using reglementDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Reglement
	 * 
	 */
	default Long count(Request<ReglementDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Reglement e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<ReglementDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		ReglementDto dto = request.getData() != null ? request.getData() : new ReglementDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (ReglementDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(ReglementDto dto, HashMap<String, java.lang.Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getNumReglement()!= null && dto.getNumReglement() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numReglement", dto.getNumReglement(), "e.numReglement", "Integer", dto.getNumReglementParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateReglement())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateReglement", dto.getDateReglement(), "e.dateReglement", "Date", dto.getDateReglementParam(), param, index, locale));
			}
			if (dto.getMontantRegle()!= null && dto.getMontantRegle() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("montantRegle", dto.getMontantRegle(), "e.montantRegle", "Integer", dto.getMontantRegleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMontantLettre())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("montantLettre", dto.getMontantLettre(), "e.montantLettre", "String", dto.getMontantLettreParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getModeReglement())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("modeReglement", dto.getModeReglement(), "e.modeReglement", "String", dto.getModeReglementParam(), param, index, locale));
			}
			if (dto.getNumCheque()!= null && dto.getNumCheque() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numCheque", dto.getNumCheque(), "e.numCheque", "Integer", dto.getNumChequeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getIdFacture()!= null && dto.getIdFacture() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idFacture", dto.getIdFacture(), "e.facture.id", "Integer", dto.getIdFactureParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
