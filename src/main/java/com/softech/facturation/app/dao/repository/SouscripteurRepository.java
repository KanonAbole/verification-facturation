package com.softech.facturation.app.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.softech.facturation.app.utils.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.contract.Request;
import com.softech.facturation.app.utils.contract.Response;
import com.softech.facturation.app.dao.entity.*;
import com.softech.facturation.app.dao.repository.customize._SouscripteurRepository;

/**
 * Repository : Souscripteur.
 */
@Repository
public interface SouscripteurRepository extends JpaRepository<Souscripteur, Integer>, _SouscripteurRepository {
	/**
	 * Finds Souscripteur by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Souscripteur whose id is equals to the given id. If
	 *         no Souscripteur is found, this method returns null.
	 */
	@Query("select e from Souscripteur e where e.id = :id and e.isDeleted = :isDeleted")
	Souscripteur findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Souscripteur by using libelle as a search criteria.
	 * 
	 * @param libelle
	 * @return An Object Souscripteur whose libelle is equals to the given libelle. If
	 *         no Souscripteur is found, this method returns null.
	 */
	@Query("select e from Souscripteur e where e.libelle = :libelle and e.isDeleted = :isDeleted")
	Souscripteur findByLibelle(@Param("libelle")String libelle, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Souscripteur by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Souscripteur whose createdBy is equals to the given createdBy. If
	 *         no Souscripteur is found, this method returns null.
	 */
	@Query("select e from Souscripteur e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Souscripteur> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Souscripteur by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Souscripteur whose updatedBy is equals to the given updatedBy. If
	 *         no Souscripteur is found, this method returns null.
	 */
	@Query("select e from Souscripteur e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Souscripteur> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Souscripteur by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Souscripteur whose createdAt is equals to the given createdAt. If
	 *         no Souscripteur is found, this method returns null.
	 */
	@Query("select e from Souscripteur e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Souscripteur> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Souscripteur by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Souscripteur whose updatedAt is equals to the given updatedAt. If
	 *         no Souscripteur is found, this method returns null.
	 */
	@Query("select e from Souscripteur e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Souscripteur> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Souscripteur by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Souscripteur whose isDeleted is equals to the given isDeleted. If
	 *         no Souscripteur is found, this method returns null.
	 */
	@Query("select e from Souscripteur e where e.isDeleted = :isDeleted")
	List<Souscripteur> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of Souscripteur by using souscripteurDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Souscripteur
	 * @throws DataAccessException,ParseException
	 */
	default List<Souscripteur> getByCriteria(Request<SouscripteurDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Souscripteur e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Souscripteur> query = em.createQuery(req, Souscripteur.class);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Souscripteur by using souscripteurDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Souscripteur
	 * 
	 */
	default Long count(Request<SouscripteurDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Souscripteur e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<SouscripteurDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		SouscripteurDto dto = request.getData() != null ? request.getData() : new SouscripteurDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (SouscripteurDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(SouscripteurDto dto, HashMap<String, java.lang.Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("libelle", dto.getLibelle(), "e.libelle", "String", dto.getLibelleParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
