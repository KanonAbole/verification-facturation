package com.softech.facturation.app.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.softech.facturation.app.utils.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.contract.Request;
import com.softech.facturation.app.utils.contract.Response;
import com.softech.facturation.app.dao.entity.*;
import com.softech.facturation.app.dao.repository.customize._FactureRepository;

/**
 * Repository : Facture.
 */
@Repository
public interface FactureRepository extends JpaRepository<Facture, Integer>, _FactureRepository {
	/**
	 * Finds Facture by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Facture whose id is equals to the given id. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.id = :id and e.isDeleted = :isDeleted")
	Facture findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Facture by using numFacture as a search criteria.
	 * 
	 * @param numFacture
	 * @return An Object Facture whose numFacture is equals to the given numFacture. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.numFacture = :numFacture and e.isDeleted = :isDeleted")
	List<Facture> findByNumFacture(@Param("numFacture")Integer numFacture, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using dateSoin as a search criteria.
	 * 
	 * @param dateSoin
	 * @return An Object Facture whose dateSoin is equals to the given dateSoin. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.dateSoin = :dateSoin and e.isDeleted = :isDeleted")
	List<Facture> findByDateSoin(@Param("dateSoin")Date dateSoin, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using numBon as a search criteria.
	 * 
	 * @param numBon
	 * @return An Object Facture whose numBon is equals to the given numBon. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.numBon = :numBon and e.isDeleted = :isDeleted")
	List<Facture> findByNumBon(@Param("numBon")Integer numBon, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using montantTtc as a search criteria.
	 * 
	 * @param montantTtc
	 * @return An Object Facture whose montantTtc is equals to the given montantTtc. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.montantTtc = :montantTtc and e.isDeleted = :isDeleted")
	List<Facture> findByMontantTtc(@Param("montantTtc")Integer montantTtc, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using tauxPcFacture as a search criteria.
	 * 
	 * @param tauxPcFacture
	 * @return An Object Facture whose tauxPcFacture is equals to the given tauxPcFacture. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.tauxPcFacture = :tauxPcFacture and e.isDeleted = :isDeleted")
	List<Facture> findByTauxPcFacture(@Param("tauxPcFacture")Integer tauxPcFacture, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using montantExclu as a search criteria.
	 * 
	 * @param montantExclu
	 * @return An Object Facture whose montantExclu is equals to the given montantExclu. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.montantExclu = :montantExclu and e.isDeleted = :isDeleted")
	List<Facture> findByMontantExclu(@Param("montantExclu")Integer montantExclu, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using montantCharge as a search criteria.
	 * 
	 * @param montantCharge
	 * @return An Object Facture whose montantCharge is equals to the given montantCharge. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.montantCharge = :montantCharge and e.isDeleted = :isDeleted")
	List<Facture> findByMontantCharge(@Param("montantCharge")Integer montantCharge, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Facture whose createdAt is equals to the given createdAt. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Facture> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using partPatient as a search criteria.
	 * 
	 * @param partPatient
	 * @return An Object Facture whose partPatient is equals to the given partPatient. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.partPatient = :partPatient and e.isDeleted = :isDeleted")
	List<Facture> findByPartPatient(@Param("partPatient")Integer partPatient, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using codeAffection as a search criteria.
	 * 
	 * @param codeAffection
	 * @return An Object Facture whose codeAffection is equals to the given codeAffection. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.codeAffection = :codeAffection and e.isDeleted = :isDeleted")
	List<Facture> findByCodeAffection(@Param("codeAffection")String codeAffection, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using numTransmission as a search criteria.
	 * 
	 * @param numTransmission
	 * @return An Object Facture whose numTransmission is equals to the given numTransmission. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.numTransmission = :numTransmission and e.isDeleted = :isDeleted")
	List<Facture> findByNumTransmission(@Param("numTransmission")Integer numTransmission, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using partAssurance as a search criteria.
	 * 
	 * @param partAssurance
	 * @return An Object Facture whose partAssurance is equals to the given partAssurance. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.partAssurance = :partAssurance and e.isDeleted = :isDeleted")
	List<Facture> findByPartAssurance(@Param("partAssurance")Integer partAssurance, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Facture whose createdBy is equals to the given createdBy. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Facture> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Facture whose updatedBy is equals to the given updatedBy. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Facture> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using codeMedPrescripteur as a search criteria.
	 * 
	 * @param codeMedPrescripteur
	 * @return An Object Facture whose codeMedPrescripteur is equals to the given codeMedPrescripteur. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.codeMedPrescripteur = :codeMedPrescripteur and e.isDeleted = :isDeleted")
	List<Facture> findByCodeMedPrescripteur(@Param("codeMedPrescripteur")String codeMedPrescripteur, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using estTraiter as a search criteria.
	 * 
	 * @param estTraiter
	 * @return An Object Facture whose estTraiter is equals to the given estTraiter. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.estTraiter = :estTraiter and e.isDeleted = :isDeleted")
	List<Facture> findByEstTraiter(@Param("estTraiter")Boolean estTraiter, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Facture whose updatedAt is equals to the given updatedAt. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Facture> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using estPayer as a search criteria.
	 * 
	 * @param estPayer
	 * @return An Object Facture whose estPayer is equals to the given estPayer. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.estPayer = :estPayer and e.isDeleted = :isDeleted")
	List<Facture> findByEstPayer(@Param("estPayer")Boolean estPayer, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Facture by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Facture whose isDeleted is equals to the given isDeleted. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.isDeleted = :isDeleted")
	List<Facture> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Facture by using idNatureActe as a search criteria.
	 * 
	 * @param idNatureActe
	 * @return An Object Facture whose idNatureActe is equals to the given idNatureActe. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.natureActe.id = :idNatureActe and e.isDeleted = :isDeleted")
	List<Facture> findByIdNatureActe(@Param("idNatureActe")Integer idNatureActe, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Facture by using idAdherent as a search criteria.
	 * 
	 * @param idAdherent
	 * @return An Object Facture whose idAdherent is equals to the given idAdherent. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.adherent.id = :idAdherent and e.isDeleted = :isDeleted")
	List<Facture> findByIdAdherent(@Param("idAdherent")Integer idAdherent, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Facture by using idPrestataire as a search criteria.
	 * 
	 * @param idPrestataire
	 * @return An Object Facture whose idPrestataire is equals to the given idPrestataire. If
	 *         no Facture is found, this method returns null.
	 */
	@Query("select e from Facture e where e.prestataire.id = :idPrestataire and e.isDeleted = :isDeleted")
	List<Facture> findByIdPrestataire(@Param("idPrestataire")Integer idPrestataire, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of Facture by using factureDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Facture
	 * @throws DataAccessException,ParseException
	 */
	default List<Facture> getByCriteria(Request<FactureDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Facture e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Facture> query = em.createQuery(req, Facture.class);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Facture by using factureDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Facture
	 * 
	 */
	default Long count(Request<FactureDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Facture e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<FactureDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		FactureDto dto = request.getData() != null ? request.getData() : new FactureDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (FactureDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(FactureDto dto, HashMap<String, java.lang.Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getNumFacture()!= null && dto.getNumFacture() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numFacture", dto.getNumFacture(), "e.numFacture", "Integer", dto.getNumFactureParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getDateSoin())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateSoin", dto.getDateSoin(), "e.dateSoin", "Date", dto.getDateSoinParam(), param, index, locale));
			}
			if (dto.getNumBon()!= null && dto.getNumBon() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numBon", dto.getNumBon(), "e.numBon", "Integer", dto.getNumBonParam(), param, index, locale));
			}
			if (dto.getMontantTtc()!= null && dto.getMontantTtc() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("montantTtc", dto.getMontantTtc(), "e.montantTtc", "Integer", dto.getMontantTtcParam(), param, index, locale));
			}
			if (dto.getTauxPcFacture()!= null && dto.getTauxPcFacture() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("tauxPcFacture", dto.getTauxPcFacture(), "e.tauxPcFacture", "Integer", dto.getTauxPcFactureParam(), param, index, locale));
			}
			if (dto.getMontantExclu()!= null && dto.getMontantExclu() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("montantExclu", dto.getMontantExclu(), "e.montantExclu", "Integer", dto.getMontantExcluParam(), param, index, locale));
			}
			if (dto.getMontantCharge()!= null && dto.getMontantCharge() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("montantCharge", dto.getMontantCharge(), "e.montantCharge", "Integer", dto.getMontantChargeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (dto.getPartPatient()!= null && dto.getPartPatient() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("partPatient", dto.getPartPatient(), "e.partPatient", "Integer", dto.getPartPatientParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCodeAffection())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("codeAffection", dto.getCodeAffection(), "e.codeAffection", "String", dto.getCodeAffectionParam(), param, index, locale));
			}
			if (dto.getNumTransmission()!= null && dto.getNumTransmission() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("numTransmission", dto.getNumTransmission(), "e.numTransmission", "Integer", dto.getNumTransmissionParam(), param, index, locale));
			}
			if (dto.getPartAssurance()!= null && dto.getPartAssurance() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("partAssurance", dto.getPartAssurance(), "e.partAssurance", "Integer", dto.getPartAssuranceParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCodeMedPrescripteur())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("codeMedPrescripteur", dto.getCodeMedPrescripteur(), "e.codeMedPrescripteur", "String", dto.getCodeMedPrescripteurParam(), param, index, locale));
			}
			if (dto.getEstTraiter()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("estTraiter", dto.getEstTraiter(), "e.estTraiter", "Boolean", dto.getEstTraiterParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getEstPayer()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("estPayer", dto.getEstPayer(), "e.estPayer", "Boolean", dto.getEstPayerParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getIdNatureActe()!= null && dto.getIdNatureActe() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idNatureActe", dto.getIdNatureActe(), "e.natureActe.id", "Integer", dto.getIdNatureActeParam(), param, index, locale));
			}
			if (dto.getIdAdherent()!= null && dto.getIdAdherent() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idAdherent", dto.getIdAdherent(), "e.adherent.id", "Integer", dto.getIdAdherentParam(), param, index, locale));
			}
			if (dto.getIdPrestataire()!= null && dto.getIdPrestataire() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("idPrestataire", dto.getIdPrestataire(), "e.prestataire.id", "Integer", dto.getIdPrestataireParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNatureActeLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("natureActeLibelle", dto.getNatureActeLibelle(), "e.natureActe.libelle", "String", dto.getNatureActeLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAdherentNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("adherentNom", dto.getAdherentNom(), "e.adherent.nom", "String", dto.getAdherentNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAdherentPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("adherentPrenom", dto.getAdherentPrenom(), "e.adherent.prenom", "String", dto.getAdherentPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPrestataireNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prestataireNom", dto.getPrestataireNom(), "e.prestataire.nom", "String", dto.getPrestataireNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPrestatairePrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prestatairePrenom", dto.getPrestatairePrenom(), "e.prestataire.prenom", "String", dto.getPrestatairePrenomParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
