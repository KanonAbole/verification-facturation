/*
 * Created on 2023-02-08 ( Time 15:51:20 )
 * Generated by Telosys Tools Generator ( version 3.3.0 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.softech.facturation.app.dao.entity;

import java.io.Serializable;

import lombok.*;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "facture"
 *
 * @author Telosys Tools Generator
 *
 */
@Data
@ToString
@Entity
@Table(name="facture" )
public class Facture implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="num_facture")
    private Integer    numFacture   ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date_soin")
    private Date       dateSoin     ;

    @Column(name="num_bon")
    private Integer    numBon       ;

    @Column(name="montant_ttc")
    private Integer    montantTtc   ;

    @Column(name="taux_pc_facture")
    private Integer    tauxPcFacture ;

    @Column(name="montant_exclu")
    private Integer    montantExclu ;

    @Column(name="montant_charge")
    private Integer    montantCharge ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at")
    private Date       createdAt    ;

    @Column(name="part_patient")
    private Integer    partPatient  ;

    @Column(name="code_affection", length=50)
    private String     codeAffection ;

    @Column(name="num_transmission")
    private Integer    numTransmission ;

    @Column(name="part_assurance")
    private Integer    partAssurance ;

    @Column(name="created_by")
    private Integer    createdBy    ;

    @Column(name="updated_by")
    private Integer    updatedBy    ;

    @Column(name="code_med_prescripteur", length=50)
    private String     codeMedPrescripteur ;

    @Column(name="est_traiter")
    private Boolean    estTraiter   ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_at")
    private Date       updatedAt    ;

    @Column(name="est_payer")
    private Boolean    estPayer     ;

    @Column(name="is_deleted")
    private Boolean    isDeleted    ;

	// "idAdherent" (column "id_adherent") is not defined by itself because used as FK in a link 
	// "idNatureActe" (column "id_nature_acte") is not defined by itself because used as FK in a link 
	// "idPrestataire" (column "id_prestataire") is not defined by itself because used as FK in a link 

    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="id_nature_acte", referencedColumnName="id")
    private NatureActe natureActe  ;
    @ManyToOne
    @JoinColumn(name="id_adherent", referencedColumnName="id")
    private Adherent adherent    ;
    @ManyToOne
    @JoinColumn(name="id_prestataire", referencedColumnName="id")
    private Prestataire prestataire ;

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public Facture() {
		super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public java.lang.Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
