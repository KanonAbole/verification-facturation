/*
 * Created on 2023-02-08 ( Time 09:09:50 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Younous. All Rights Reserved.
 */

package com.softech.facturation.app.rest.errorhandler;


import java.io.IOException;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import com.softech.facturation.app.utils.FunctionalError;

/**
 * CustomHttpErrorResponse
 * 
 * @author Younous
 *
 */

//@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@RestController
public class CustomHttpErrorResponse extends ResponseEntityExceptionHandler{


	private Logger slf4jLogger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private FunctionalError functionalError;
		
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, org.springframework.http.HttpHeaders headers, HttpStatus status, WebRequest request)  {



	  Locale locale = new Locale("fr", "");
      HttpError errors = new HttpError();
      errors.setStatus(functionalError.MALFORMED_REQUEST(ex.getMessage(), locale));
      
      return handleExceptionInternal(ex, errors, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(
			NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		 Locale locale = new Locale("fr", "");
	      HttpError errors = new HttpError();
	      
	
	      
	      errors.setStatus(functionalError.NO_HANDLER_FOUND(ex.getMessage(), locale));
	      

	      
	      String responseValue = new Gson().toJson(errors, HttpError.class);
	      
	      ObjectMapper mapper = new ObjectMapper();
	      try {
			Map<String,Object> map = mapper.readValue(responseValue, Map.class); 
	      
	      return handleExceptionInternal(ex, map, headers, status, request);
	      
	      } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	      return handleExceptionInternal(ex, responseValue, headers, status, request);
	}

}
