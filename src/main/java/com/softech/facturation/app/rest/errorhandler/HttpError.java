/*
 * Created on 2023-02-08 ( Time 09:09:50 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Younous. All Rights Reserved.
 */

package com.softech.facturation.app.rest.errorhandler;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.softech.facturation.app.utils.Status;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * HttpError
 * 
 * @author Younous
 *
 */

@XmlRootElement

@JsonInclude(Include.NON_NULL)
public class HttpError {
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime timestamp;
    private Status	status = new Status();
    private String error;
    private String path;
    private String message;
    
//     HttpError() {
//    }
//
//    HttpError(Status status) {
//        this();
//        this.status = status;
//    }
//
//    HttpError(Status status, Throwable ex) {
//        this();
//        this.status = status;
//        this.message = "Unexpected error";
//        //this.debugMessage = ex.getLocalizedMessage();
//    }
//
//    HttpError(Status status, String message, Throwable ex) {
//        this();
//        this.status = status;
//        this.message = message;
//        //this.debugMessage = ex.getLocalizedMessage();
//    }
	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status2) {
		this.status = status2;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

}
