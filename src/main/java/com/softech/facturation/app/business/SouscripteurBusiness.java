



                                    								

/*
 * Java transformer for entity table souscripteur 
 * Created on 2023-02-08 ( Time 14:31:44 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;

import java.util.Arrays;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.facturation.app.utils.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.utils.enums.*;
import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.contract.IBasicBusiness;
import com.softech.facturation.app.utils.contract.Request;
import com.softech.facturation.app.utils.contract.Response;
import com.softech.facturation.app.utils.dto.transformer.*;
import com.softech.facturation.app.dao.entity.Souscripteur;
import com.softech.facturation.app.dao.entity.*;
import com.softech.facturation.app.dao.repository.*;

/**
BUSINESS for table "souscripteur"
 * 
 * @author Geo
 *
 */
@Log
@Component
public class SouscripteurBusiness implements IBasicBusiness<Request<SouscripteurDto>, Response<SouscripteurDto>> {

	private Response<SouscripteurDto> response;
	@Autowired
	private SouscripteurRepository souscripteurRepository;
	@Autowired
	private AdherentRepository adherentRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

	public SouscripteurBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	
	/**
	 * create Souscripteur by using SouscripteurDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscripteurDto> create(Request<SouscripteurDto> request, Locale locale)  throws DataAccessException, Exception {
		
		response = new Response<SouscripteurDto>();
			
			List<Souscripteur> items = new ArrayList<Souscripteur>();
			
			for (SouscripteurDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if souscripteur to insert do not exist
				Souscripteur existingEntity = null;
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("souscripteur id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// verif unique libelle in db
				existingEntity = souscripteurRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("souscripteur libelle -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				// verif unique libelle in items to save
				if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE(" libelle ", locale));
					response.setHasError(true);
					return response;
				}

				Souscripteur entityToSave = null;
				entityToSave = SouscripteurTransformer.INSTANCE.toEntity(dto);
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}
        saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * update Souscripteur by using SouscripteurDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscripteurDto> update(Request<SouscripteurDto> request, Locale locale)  throws DataAccessException, Exception {
		
		response = new Response<SouscripteurDto>();

			List<Souscripteur> items = new ArrayList<Souscripteur>();
			
			for (SouscripteurDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la souscripteur existe
				Souscripteur entityToSave = null;
				entityToSave = souscripteurRepository.findOne(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscripteur id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				if (Utilities.notBlank(dto.getLibelle())) {
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

		saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * delete Souscripteur by using SouscripteurDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<SouscripteurDto> delete(Request<SouscripteurDto> request, Locale locale)  {
		
		response = new Response<SouscripteurDto>();

			List<Souscripteur> items = new ArrayList<Souscripteur>();
			
			for (SouscripteurDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la souscripteur existe
				Souscripteur existingEntity = null;
				existingEntity = souscripteurRepository.findOne(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("souscripteur -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// adherent
				List<Adherent> listOfAdherent = adherentRepository.findByIdSouscripteur(existingEntity.getId(), false);
				if (listOfAdherent != null && !listOfAdherent.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfAdherent.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				souscripteurRepository.saveAll((Iterable<Souscripteur>) items);

				response.setHasError(false);
			}
		return response;
	}

	/**
	 * get Souscripteur by using SouscripteurDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<SouscripteurDto> getByCriteria(Request<SouscripteurDto> request, Locale locale) throws DataAccessException, Exception{
		
		response = new Response<SouscripteurDto>();

			List<Souscripteur> items = null;
			items = souscripteurRepository.getByCriteria(request, em, locale);
			saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * get full SouscripteurDto by using Souscripteur as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @throws ParseException 
	 * @return
	 * @throws Exception 
	 * @throws DataAccessException 
	 */
	private SouscripteurDto getFullInfos(SouscripteurDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}

    /**
	 * @param request
	 * @param locale
	 * @param items
	 * @throws ParseException
	 */
	private void saveAndTransform(Request<SouscripteurDto> request, Locale locale, List<Souscripteur> items) throws DataAccessException, Exception{

    if (items != null && !items.isEmpty()) {
				List<Souscripteur> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = souscripteurRepository.saveAll((Iterable<Souscripteur>) items);
				if (itemsSaved != null) {
				List<SouscripteurDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? SouscripteurTransformer.INSTANCE.toLiteDtos(itemsSaved) : SouscripteurTransformer.INSTANCE.toDtos(itemsSaved);
				
				final int size = itemsSaved.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);

			    response.setCount(souscripteurRepository.count(request, em, locale));
                  
				response.setHasError(false);
				}
			}
    }

}
