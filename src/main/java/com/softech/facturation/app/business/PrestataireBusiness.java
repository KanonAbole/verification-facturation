



                                                											

/*
 * Java transformer for entity table prestataire 
 * Created on 2023-02-08 ( Time 10:54:49 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;

import java.util.Arrays;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.facturation.app.utils.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.utils.enums.*;
import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.contract.IBasicBusiness;
import com.softech.facturation.app.utils.contract.Request;
import com.softech.facturation.app.utils.contract.Response;
import com.softech.facturation.app.utils.dto.transformer.*;
import com.softech.facturation.app.dao.entity.Prestataire;
import com.softech.facturation.app.dao.entity.*;
import com.softech.facturation.app.dao.repository.*;

/**
BUSINESS for table "prestataire"
 * 
 * @author Geo
 *
 */
@Log
@Component
public class PrestataireBusiness implements IBasicBusiness<Request<PrestataireDto>, Response<PrestataireDto>> {

	private Response<PrestataireDto> response;
	@Autowired
	private PrestataireRepository prestataireRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

	public PrestataireBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	
	/**
	 * create Prestataire by using PrestataireDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PrestataireDto> create(Request<PrestataireDto> request, Locale locale)  throws DataAccessException, Exception {
		
		response = new Response<PrestataireDto>();
			
			List<Prestataire> items = new ArrayList<Prestataire>();
			
			for (PrestataireDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("codePrestataire", dto.getCodePrestataire());
				fieldsToVerify.put("nom", dto.getNom());
				//fieldsToVerify.put("prenom", dto.getPrenom());
				fieldsToVerify.put("contact", dto.getContact());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if prestataire to insert do not exist
				Prestataire existingEntity = null;
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("prestataire id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				Prestataire entityToSave = null;
				entityToSave = PrestataireTransformer.INSTANCE.toEntity(dto);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}
        saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * update Prestataire by using PrestataireDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PrestataireDto> update(Request<PrestataireDto> request, Locale locale)  throws DataAccessException, Exception {
		
		response = new Response<PrestataireDto>();

			List<Prestataire> items = new ArrayList<Prestataire>();
			
			for (PrestataireDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la prestataire existe
				Prestataire entityToSave = null;
				entityToSave = prestataireRepository.findOne(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("prestataire id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				if (Utilities.notBlank(dto.getCodePrestataire())) {
					entityToSave.setCodePrestataire(dto.getCodePrestataire());
				}
				if (Utilities.notBlank(dto.getNom())) {
					entityToSave.setNom(dto.getNom());
				}
				if (Utilities.notBlank(dto.getPrenom())) {
					entityToSave.setPrenom(dto.getPrenom());
				}
				if (Utilities.notBlank(dto.getContact())) {
					entityToSave.setContact(dto.getContact());
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

		saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * delete Prestataire by using PrestataireDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<PrestataireDto> delete(Request<PrestataireDto> request, Locale locale)  {
		
		response = new Response<PrestataireDto>();

			List<Prestataire> items = new ArrayList<Prestataire>();
			
			for (PrestataireDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la prestataire existe
				Prestataire existingEntity = null;
				existingEntity = prestataireRepository.findOne(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("prestataire -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				prestataireRepository.saveAll((Iterable<Prestataire>) items);

				response.setHasError(false);
			}
		return response;
	}

	/**
	 * get Prestataire by using PrestataireDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<PrestataireDto> getByCriteria(Request<PrestataireDto> request, Locale locale) throws DataAccessException, Exception{
		
		response = new Response<PrestataireDto>();

			List<Prestataire> items = null;
			items = prestataireRepository.getByCriteria(request, em, locale);
			saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * get full PrestataireDto by using Prestataire as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @throws ParseException 
	 * @return
	 * @throws Exception 
	 * @throws DataAccessException 
	 */
	private PrestataireDto getFullInfos(PrestataireDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}

    /**
	 * @param request
	 * @param locale
	 * @param items
	 * @throws ParseException
	 */
	private void saveAndTransform(Request<PrestataireDto> request, Locale locale, List<Prestataire> items) throws DataAccessException, Exception{

    if (items != null && !items.isEmpty()) {
				List<Prestataire> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = prestataireRepository.saveAll((Iterable<Prestataire>) items);
				if (itemsSaved != null) {
				List<PrestataireDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? PrestataireTransformer.INSTANCE.toLiteDtos(itemsSaved) : PrestataireTransformer.INSTANCE.toDtos(itemsSaved);
				
				final int size = itemsSaved.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);

			    response.setCount(prestataireRepository.count(request, em, locale));
                  
				response.setHasError(false);
				}
			}
    }

}
