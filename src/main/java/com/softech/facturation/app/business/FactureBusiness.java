



                                                                                                    																								

/*
 * Java transformer for entity table facture 
 * Created on 2023-02-08 ( Time 15:51:20 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;

import java.util.Arrays;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.facturation.app.utils.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.utils.enums.*;
import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.contract.IBasicBusiness;
import com.softech.facturation.app.utils.contract.Request;
import com.softech.facturation.app.utils.contract.Response;
import com.softech.facturation.app.utils.dto.transformer.*;
import com.softech.facturation.app.dao.entity.Facture;
import com.softech.facturation.app.dao.entity.NatureActe;
import com.softech.facturation.app.dao.entity.Adherent;
import com.softech.facturation.app.dao.entity.Prestataire;
import com.softech.facturation.app.dao.entity.*;
import com.softech.facturation.app.dao.repository.*;

/**
BUSINESS for table "facture"
 * 
 * @author Geo
 *
 */
@Log
@Component
public class FactureBusiness implements IBasicBusiness<Request<FactureDto>, Response<FactureDto>> {

	private Response<FactureDto> response;
	@Autowired
	private FactureRepository factureRepository;
	@Autowired
	private NatureActeRepository natureActeRepository;
	@Autowired
	private AdherentRepository adherentRepository;
	@Autowired
	private PrestataireRepository prestataireRepository;
	@Autowired
	private ReglementRepository reglementRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

	public FactureBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	
	/**
	 * create Facture by using FactureDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<FactureDto> create(Request<FactureDto> request, Locale locale)  throws DataAccessException, Exception {
		
		response = new Response<FactureDto>();
			
			List<Facture> items = new ArrayList<Facture>();
			
			for (FactureDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("numFacture", dto.getNumFacture());
				fieldsToVerify.put("dateSoin", dto.getDateSoin());
				fieldsToVerify.put("numBon", dto.getNumBon());
				fieldsToVerify.put("montantTtc", dto.getMontantTtc());
				fieldsToVerify.put("tauxPcFacture", dto.getTauxPcFacture());
				//fieldsToVerify.put("montantExclu", dto.getMontantExclu());
				//fieldsToVerify.put("montantCharge", dto.getMontantCharge());
				fieldsToVerify.put("partPatient", dto.getPartPatient());
				fieldsToVerify.put("codeAffection", dto.getCodeAffection());
				//fieldsToVerify.put("numTransmission", dto.getNumTransmission());
				fieldsToVerify.put("partAssurance", dto.getPartAssurance());
				fieldsToVerify.put("codeMedPrescripteur", dto.getCodeMedPrescripteur());
				fieldsToVerify.put("estTraiter", dto.getEstTraiter());
				fieldsToVerify.put("estPayer", dto.getEstPayer());
				fieldsToVerify.put("idAdherent", dto.getIdAdherent());
				fieldsToVerify.put("idNatureActe", dto.getIdNatureActe());
				fieldsToVerify.put("idPrestataire", dto.getIdPrestataire());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if facture to insert do not exist
				Facture existingEntity = null;
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("facture id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if natureActe exist
				NatureActe existingNatureActe = null;
				if (dto.getIdNatureActe() != null && dto.getIdNatureActe() > 0){
					existingNatureActe = natureActeRepository.findOne(dto.getIdNatureActe(), false);
					if (existingNatureActe == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("natureActe idNatureActe -> " + dto.getIdNatureActe(), locale));
						response.setHasError(true);
						return response;
					}
				}
				// Verify if adherent exist
				Adherent existingAdherent = null;
				if (dto.getIdAdherent() != null && dto.getIdAdherent() > 0){
					existingAdherent = adherentRepository.findOne(dto.getIdAdherent(), false);
					if (existingAdherent == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("adherent idAdherent -> " + dto.getIdAdherent(), locale));
						response.setHasError(true);
						return response;
					}
				}
				// Verify if prestataire exist
				Prestataire existingPrestataire = null;
				if (dto.getIdPrestataire() != null && dto.getIdPrestataire() > 0){
					existingPrestataire = prestataireRepository.findOne(dto.getIdPrestataire(), false);
					if (existingPrestataire == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("prestataire idPrestataire -> " + dto.getIdPrestataire(), locale));
						response.setHasError(true);
						return response;
					}
				}
				Facture entityToSave = null;
				entityToSave = FactureTransformer.INSTANCE.toEntity(dto, existingNatureActe, existingAdherent, existingPrestataire);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}
        saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * update Facture by using FactureDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<FactureDto> update(Request<FactureDto> request, Locale locale)  throws DataAccessException, Exception {
		
		response = new Response<FactureDto>();

			List<Facture> items = new ArrayList<Facture>();
			
			for (FactureDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la facture existe
				Facture entityToSave = null;
				entityToSave = factureRepository.findOne(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("facture id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if natureActe exist
				if (dto.getIdNatureActe() != null && dto.getIdNatureActe() > 0){
					NatureActe existingNatureActe = natureActeRepository.findOne(dto.getIdNatureActe(), false);
					if (existingNatureActe == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("natureActe idNatureActe -> " + dto.getIdNatureActe(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setNatureActe(existingNatureActe);
				}
				// Verify if adherent exist
				if (dto.getIdAdherent() != null && dto.getIdAdherent() > 0){
					Adherent existingAdherent = adherentRepository.findOne(dto.getIdAdherent(), false);
					if (existingAdherent == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("adherent idAdherent -> " + dto.getIdAdherent(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setAdherent(existingAdherent);
				}
				// Verify if prestataire exist
				if (dto.getIdPrestataire() != null && dto.getIdPrestataire() > 0){
					Prestataire existingPrestataire = prestataireRepository.findOne(dto.getIdPrestataire(), false);
					if (existingPrestataire == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("prestataire idPrestataire -> " + dto.getIdPrestataire(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setPrestataire(existingPrestataire);
				}
				if (dto.getNumFacture() != null && dto.getNumFacture() > 0) {
					entityToSave.setNumFacture(dto.getNumFacture());
				}
				if (Utilities.notBlank(dto.getDateSoin())) {
					entityToSave.setDateSoin(dateFormat.parse(dto.getDateSoin()));
				}
				if (dto.getNumBon() != null && dto.getNumBon() > 0) {
					entityToSave.setNumBon(dto.getNumBon());
				}
				if (dto.getMontantTtc() != null && dto.getMontantTtc() > 0) {
					entityToSave.setMontantTtc(dto.getMontantTtc());
				}
				if (dto.getTauxPcFacture() != null && dto.getTauxPcFacture() > 0) {
					entityToSave.setTauxPcFacture(dto.getTauxPcFacture());
				}
				if (dto.getMontantExclu() != null && dto.getMontantExclu() > 0) {
					entityToSave.setMontantExclu(dto.getMontantExclu());
				}
				if (dto.getMontantCharge() != null && dto.getMontantCharge() > 0) {
					entityToSave.setMontantCharge(dto.getMontantCharge());
				}
				if (dto.getPartPatient() != null && dto.getPartPatient() > 0) {
					entityToSave.setPartPatient(dto.getPartPatient());
				}
				if (Utilities.notBlank(dto.getCodeAffection())) {
					entityToSave.setCodeAffection(dto.getCodeAffection());
				}
				if (dto.getNumTransmission() != null && dto.getNumTransmission() > 0) {
					entityToSave.setNumTransmission(dto.getNumTransmission());
				}
				if (dto.getPartAssurance() != null && dto.getPartAssurance() > 0) {
					entityToSave.setPartAssurance(dto.getPartAssurance());
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				if (Utilities.notBlank(dto.getCodeMedPrescripteur())) {
					entityToSave.setCodeMedPrescripteur(dto.getCodeMedPrescripteur());
				}
				if (dto.getEstTraiter() != null) {
					entityToSave.setEstTraiter(dto.getEstTraiter());
				}
				if (dto.getEstPayer() != null) {
					entityToSave.setEstPayer(dto.getEstPayer());
				}
				entityToSave.setUpdatedBy(request.getUser());
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				items.add(entityToSave);
			}

		saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * delete Facture by using FactureDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<FactureDto> delete(Request<FactureDto> request, Locale locale)  {
		
		response = new Response<FactureDto>();

			List<Facture> items = new ArrayList<Facture>();
			
			for (FactureDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la facture existe
				Facture existingEntity = null;
				existingEntity = factureRepository.findOne(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("facture -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// reglement
				List<Reglement> listOfReglement = reglementRepository.findByIdFacture(existingEntity.getId(), false);
				if (listOfReglement != null && !listOfReglement.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfReglement.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				factureRepository.saveAll((Iterable<Facture>) items);

				response.setHasError(false);
			}
		return response;
	}

	/**
	 * get Facture by using FactureDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<FactureDto> getByCriteria(Request<FactureDto> request, Locale locale) throws DataAccessException, Exception{
		
		response = new Response<FactureDto>();

			List<Facture> items = null;
			items = factureRepository.getByCriteria(request, em, locale);
			saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * get full FactureDto by using Facture as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @throws ParseException 
	 * @return
	 * @throws Exception 
	 * @throws DataAccessException 
	 */
	private FactureDto getFullInfos(FactureDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here
		
		Prestataire prestataire = prestataireRepository.findOne(dto.getIdPrestataire(), Boolean.FALSE);
		Adherent adherent = adherentRepository.findOne(dto.getIdAdherent(), Boolean.FALSE);
		NatureActe natureActe = natureActeRepository.findOne(dto.getIdNatureActe(), Boolean.FALSE);
		
		if(prestataire != null) {
			dto.setCodePrestataire(prestataire.getCodePrestataire());
		}
		if(adherent != null) {
			dto.setMatriculeAdherent(adherent.getMatricule());
		}
		if(natureActe != null) {
			dto.setLibelleActe(natureActe.getLibelle());
		}


		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}

    /**
	 * @param request
	 * @param locale
	 * @param items
	 * @throws ParseException
	 */
	private void saveAndTransform(Request<FactureDto> request, Locale locale, List<Facture> items) throws DataAccessException, Exception{

    if (items != null && !items.isEmpty()) {
				List<Facture> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = factureRepository.saveAll((Iterable<Facture>) items);
				if (itemsSaved != null) {
				List<FactureDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? FactureTransformer.INSTANCE.toLiteDtos(itemsSaved) : FactureTransformer.INSTANCE.toDtos(itemsSaved);
				
				final int size = itemsSaved.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);

			    response.setCount(factureRepository.count(request, em, locale));
                  
				response.setHasError(false);
				}
			}
    }

}
