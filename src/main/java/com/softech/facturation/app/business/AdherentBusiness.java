



                                                                                        																					

/*
 * Java transformer for entity table adherent 
 * Created on 2023-02-08 ( Time 14:31:41 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;

import java.util.Arrays;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.facturation.app.utils.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.utils.enums.*;
import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.contract.IBasicBusiness;
import com.softech.facturation.app.utils.contract.Request;
import com.softech.facturation.app.utils.contract.Response;
import com.softech.facturation.app.utils.dto.transformer.*;
import com.softech.facturation.app.dao.entity.Adherent;
import com.softech.facturation.app.dao.entity.Souscripteur;
import com.softech.facturation.app.dao.entity.*;
import com.softech.facturation.app.dao.repository.*;

/**
BUSINESS for table "adherent"
 * 
 * @author Geo
 *
 */
@Log
@Component
public class AdherentBusiness implements IBasicBusiness<Request<AdherentDto>, Response<AdherentDto>> {

	private Response<AdherentDto> response;
	@Autowired
	private AdherentRepository adherentRepository;
	@Autowired
	private SouscripteurRepository souscripteurRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@Autowired
	private SouscripteurRepository souscripteurRespository;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

	public AdherentBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	
	/**
	 * create Adherent by using AdherentDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AdherentDto> create(Request<AdherentDto> request, Locale locale)  throws DataAccessException, Exception {
		
		response = new Response<AdherentDto>();
			
			List<Adherent> items = new ArrayList<Adherent>();
			
			for (AdherentDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("matricule", dto.getMatricule());
				fieldsToVerify.put("numAdherent", dto.getNumAdherent());
				fieldsToVerify.put("tauxPriseCharge", dto.getTauxPriseCharge());
				fieldsToVerify.put("nom", dto.getNom());
				fieldsToVerify.put("prenom", dto.getPrenom());
				fieldsToVerify.put("dateNaissance", dto.getDateNaissance());
				fieldsToVerify.put("lieuNaissance", dto.getLieuNaissance());
				fieldsToVerify.put("agent", dto.getAgent());
				//fieldsToVerify.put("fonction", dto.getFonction());
				fieldsToVerify.put("telephone1", dto.getTelephone1());
				//fieldsToVerify.put("telephone2", dto.getTelephone2());
				fieldsToVerify.put("sexe", dto.getSexe());
				fieldsToVerify.put("photo", dto.getPhoto());
				//fieldsToVerify.put("idSouscripteur", dto.getIdSouscripteur());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if adherent to insert do not exist
				Adherent existingEntity = null;
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("adherent id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// verif unique matricule in db
				existingEntity = adherentRepository.findByMatricule(dto.getMatricule(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("adherent matricule -> " + dto.getMatricule(), locale));
					response.setHasError(true);
					return response;
				}
				// verif unique matricule in items to save
				if (items.stream().anyMatch(a -> a.getMatricule().equalsIgnoreCase(dto.getMatricule()))) {
					response.setStatus(functionalError.DATA_DUPLICATE(" matricule ", locale));
					response.setHasError(true);
					return response;
				}

				// Verify if souscripteur exist
				Souscripteur existingSouscripteur = null;
				if (dto.getIdSouscripteur() != null && dto.getIdSouscripteur() > 0){
					existingSouscripteur = souscripteurRepository.findOne(dto.getIdSouscripteur(), false);
					if (existingSouscripteur == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("souscripteur idSouscripteur -> " + dto.getIdSouscripteur(), locale));
						response.setHasError(true);
						return response;
					}
				}
				Adherent entityToSave = null;
				entityToSave = AdherentTransformer.INSTANCE.toEntity(dto, existingSouscripteur);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}
        saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * update Adherent by using AdherentDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AdherentDto> update(Request<AdherentDto> request, Locale locale)  throws DataAccessException, Exception {
		
		response = new Response<AdherentDto>();

			List<Adherent> items = new ArrayList<Adherent>();
			
			for (AdherentDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la adherent existe
				Adherent entityToSave = null;
				entityToSave = adherentRepository.findOne(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("adherent id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if souscripteur exist
				if (dto.getIdSouscripteur() != null && dto.getIdSouscripteur() > 0){
					Souscripteur existingSouscripteur = souscripteurRepository.findOne(dto.getIdSouscripteur(), false);
					if (existingSouscripteur == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("souscripteur idSouscripteur -> " + dto.getIdSouscripteur(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setSouscripteur(existingSouscripteur);
				}
				if (Utilities.notBlank(dto.getMatricule())) {
					entityToSave.setMatricule(dto.getMatricule());
				}
				if (Utilities.notBlank(dto.getNumAdherent())) {
					entityToSave.setNumAdherent(dto.getNumAdherent());
				}
				if (dto.getTauxPriseCharge() != null && dto.getTauxPriseCharge() > 0) {
					entityToSave.setTauxPriseCharge(dto.getTauxPriseCharge());
				}
				if (Utilities.notBlank(dto.getNom())) {
					entityToSave.setNom(dto.getNom());
				}
				if (Utilities.notBlank(dto.getPrenom())) {
					entityToSave.setPrenom(dto.getPrenom());
				}
				if (Utilities.notBlank(dto.getDateNaissance())) {
					entityToSave.setDateNaissance(dateFormat.parse(dto.getDateNaissance()));
				}
				if (Utilities.notBlank(dto.getLieuNaissance())) {
					entityToSave.setLieuNaissance(dto.getLieuNaissance());
				}
				if (dto.getAgent() != null) {
					entityToSave.setAgent(dto.getAgent());
				}
				if (Utilities.notBlank(dto.getFonction())) {
					entityToSave.setFonction(dto.getFonction());
				}
				if (Utilities.notBlank(dto.getTelephone1())) {
					entityToSave.setTelephone1(dto.getTelephone1());
				}
				if (Utilities.notBlank(dto.getTelephone2())) {
					entityToSave.setTelephone2(dto.getTelephone2());
				}
				if (Utilities.notBlank(dto.getSexe())) {
					entityToSave.setSexe(dto.getSexe());
				}
				if (Utilities.notBlank(dto.getPhoto())) {
					entityToSave.setPhoto(dto.getPhoto());
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

		saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * delete Adherent by using AdherentDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AdherentDto> delete(Request<AdherentDto> request, Locale locale)  {
		
		response = new Response<AdherentDto>();

			List<Adherent> items = new ArrayList<Adherent>();
			
			for (AdherentDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la adherent existe
				Adherent existingEntity = null;
				existingEntity = adherentRepository.findOne(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("adherent -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				adherentRepository.saveAll((Iterable<Adherent>) items);

				response.setHasError(false);
			}
		return response;
	}

	/**
	 * get Adherent by using AdherentDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<AdherentDto> getByCriteria(Request<AdherentDto> request, Locale locale) throws DataAccessException, Exception{
		
		response = new Response<AdherentDto>();

			List<Adherent> items = null;
			items = adherentRepository.getByCriteria(request, em, locale);
			saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * get full AdherentDto by using Adherent as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @throws ParseException 
	 * @return
	 * @throws Exception 
	 * @throws DataAccessException 
	 */
	private AdherentDto getFullInfos(AdherentDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here
		Souscripteur souscripteur = souscripteurRespository.findOne(dto.getIdSouscripteur(), Boolean.FALSE);
		
		if(souscripteur != null) {
			dto.setLibelleSouscripteur(souscripteur.getLibelle());
		}

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}

    /**
	 * @param request
	 * @param locale
	 * @param items
	 * @throws ParseException
	 */
	private void saveAndTransform(Request<AdherentDto> request, Locale locale, List<Adherent> items) throws DataAccessException, Exception{

    if (items != null && !items.isEmpty()) {
				List<Adherent> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = adherentRepository.saveAll((Iterable<Adherent>) items);
				if (itemsSaved != null) {
				List<AdherentDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? AdherentTransformer.INSTANCE.toLiteDtos(itemsSaved) : AdherentTransformer.INSTANCE.toDtos(itemsSaved);
				
				final int size = itemsSaved.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);

			    response.setCount(adherentRepository.count(request, em, locale));
                  
				response.setHasError(false);
				}
			}
    }

}
