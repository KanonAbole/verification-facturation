



                                                            														

/*
 * Java transformer for entity table reglement 
 * Created on 2023-02-13 ( Time 13:07:55 )
 * Generator tool : Telosys Tools Generator ( version 3.3.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package com.softech.facturation.app.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;

import java.util.Arrays;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.facturation.app.utils.*;
import com.softech.facturation.app.utils.dto.*;
import com.softech.facturation.app.utils.enums.*;
import com.softech.facturation.app.utils.contract.*;
import com.softech.facturation.app.utils.contract.IBasicBusiness;
import com.softech.facturation.app.utils.contract.Request;
import com.softech.facturation.app.utils.contract.Response;
import com.softech.facturation.app.utils.dto.transformer.*;
import com.softech.facturation.app.dao.entity.Reglement;
import com.softech.facturation.app.dao.entity.Facture;
import com.softech.facturation.app.dao.entity.*;
import com.softech.facturation.app.dao.repository.*;

/**
BUSINESS for table "reglement"
 * 
 * @author Geo
 *
 */
@Log
@Component
public class ReglementBusiness implements IBasicBusiness<Request<ReglementDto>, Response<ReglementDto>> {

	private Response<ReglementDto> response;
	@Autowired
	private ReglementRepository reglementRepository;
	@Autowired
	private FactureRepository factureRepository;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

	public ReglementBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	
	/**
	 * create Reglement by using ReglementDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ReglementDto> create(Request<ReglementDto> request, Locale locale)  throws DataAccessException, Exception {
		
		response = new Response<ReglementDto>();
			
			List<Reglement> items = new ArrayList<Reglement>();
			
			for (ReglementDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("numReglement", dto.getNumReglement());
				fieldsToVerify.put("dateReglement", dto.getDateReglement());
				fieldsToVerify.put("montantRegle", dto.getMontantRegle());
				fieldsToVerify.put("montantLettre", dto.getMontantLettre());
				fieldsToVerify.put("modeReglement", dto.getModeReglement());
				fieldsToVerify.put("numCheque", dto.getNumCheque());
				fieldsToVerify.put("idFacture", dto.getIdFacture());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if reglement to insert do not exist
				Reglement existingEntity = null;
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("reglement id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if facture exist
				Facture existingFacture = null;
				if (dto.getIdFacture() != null && dto.getIdFacture() > 0){
					existingFacture = factureRepository.findOne(dto.getIdFacture(), false);
					if (existingFacture == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("facture idFacture -> " + dto.getIdFacture(), locale));
						response.setHasError(true);
						return response;
					}
					existingFacture.setEstPayer(true);
				}
				Reglement entityToSave = null;
				entityToSave = ReglementTransformer.INSTANCE.toEntity(dto, existingFacture);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}
        saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * update Reglement by using ReglementDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ReglementDto> update(Request<ReglementDto> request, Locale locale)  throws DataAccessException, Exception {
		
		response = new Response<ReglementDto>();

			List<Reglement> items = new ArrayList<Reglement>();
			
			for (ReglementDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la reglement existe
				Reglement entityToSave = null;
				entityToSave = reglementRepository.findOne(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("reglement id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if facture exist
				if (dto.getIdFacture() != null && dto.getIdFacture() > 0){
					Facture existingFacture = factureRepository.findOne(dto.getIdFacture(), false);
					if (existingFacture == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("facture idFacture -> " + dto.getIdFacture(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setFacture(existingFacture);
				}
				if (dto.getNumReglement() != null && dto.getNumReglement() > 0) {
					entityToSave.setNumReglement(dto.getNumReglement());
				}
				if (Utilities.notBlank(dto.getDateReglement())) {
					entityToSave.setDateReglement(dateFormat.parse(dto.getDateReglement()));
				}
				if (dto.getMontantRegle() != null && dto.getMontantRegle() > 0) {
					entityToSave.setMontantRegle(dto.getMontantRegle());
				}
				if (Utilities.notBlank(dto.getMontantLettre())) {
					entityToSave.setMontantLettre(dto.getMontantLettre());
				}
				if (Utilities.notBlank(dto.getModeReglement())) {
					entityToSave.setModeReglement(dto.getModeReglement());
				}
				if (dto.getNumCheque() != null && dto.getNumCheque() > 0) {
					entityToSave.setNumCheque(dto.getNumCheque());
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

		saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * delete Reglement by using ReglementDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<ReglementDto> delete(Request<ReglementDto> request, Locale locale)  {
		
		response = new Response<ReglementDto>();

			List<Reglement> items = new ArrayList<Reglement>();
			
			for (ReglementDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la reglement existe
				Reglement existingEntity = null;
				existingEntity = reglementRepository.findOne(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("reglement -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				reglementRepository.saveAll((Iterable<Reglement>) items);

				response.setHasError(false);
			}
		return response;
	}

	/**
	 * get Reglement by using ReglementDto as object.
	 * 
	 * @param request
	 * @return response
	 * @throws ParseException 
	 * @throws Exception 
	 * @throws DataAccessException 
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<ReglementDto> getByCriteria(Request<ReglementDto> request, Locale locale) throws DataAccessException, Exception{
		
		response = new Response<ReglementDto>();

			List<Reglement> items = null;
			items = reglementRepository.getByCriteria(request, em, locale);
			saveAndTransform(request, locale, items);
		return response;
	}

	/**
	 * get full ReglementDto by using Reglement as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @throws ParseException 
	 * @return
	 * @throws Exception 
	 * @throws DataAccessException 
	 */
	private ReglementDto getFullInfos(ReglementDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		Facture facture = factureRepository.findOne(dto.getIdFacture(), Boolean.FALSE);
		if(facture != null) {
			dto.setNumFacture(facture.getNumFacture());
		}
		
		
		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}

    /**
	 * @param request
	 * @param locale
	 * @param items
	 * @throws ParseException
	 */
	private void saveAndTransform(Request<ReglementDto> request, Locale locale, List<Reglement> items) throws DataAccessException, Exception{

    if (items != null && !items.isEmpty()) {
				List<Reglement> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = reglementRepository.saveAll((Iterable<Reglement>) items);
				if (itemsSaved != null) {
				List<ReglementDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? ReglementTransformer.INSTANCE.toLiteDtos(itemsSaved) : ReglementTransformer.INSTANCE.toDtos(itemsSaved);
				
				final int size = itemsSaved.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);

			    response.setCount(reglementRepository.count(request, em, locale));
                  
				response.setHasError(false);
				}
			}
    }

}
