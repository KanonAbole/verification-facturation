package com.softech.facturation.app.utils.dto.transformer;

import com.softech.facturation.app.dao.entity.Prestataire;
import com.softech.facturation.app.utils.dto.PrestataireDto;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-15T12:06:42+0000",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_352 (Private Build)"
)
public class PrestataireTransformerImpl implements PrestataireTransformer {

    @Override
    public PrestataireDto toDto(Prestataire entity) throws ParseException {
        if ( entity == null ) {
            return null;
        }

        PrestataireDto prestataireDto = new PrestataireDto();

        if ( entity.getCreatedAt() != null ) {
            prestataireDto.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getCreatedAt() ) );
        }
        if ( entity.getUpdatedAt() != null ) {
            prestataireDto.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getUpdatedAt() ) );
        }
        prestataireDto.setId( entity.getId() );
        prestataireDto.setCodePrestataire( entity.getCodePrestataire() );
        prestataireDto.setNom( entity.getNom() );
        prestataireDto.setPrenom( entity.getPrenom() );
        prestataireDto.setContact( entity.getContact() );
        prestataireDto.setCreatedBy( entity.getCreatedBy() );
        prestataireDto.setUpdatedBy( entity.getUpdatedBy() );
        prestataireDto.setIsDeleted( entity.getIsDeleted() );

        return prestataireDto;
    }

    @Override
    public List<PrestataireDto> toDtos(List<Prestataire> entities) throws ParseException {
        if ( entities == null ) {
            return null;
        }

        List<PrestataireDto> list = new ArrayList<PrestataireDto>( entities.size() );
        for ( Prestataire prestataire : entities ) {
            list.add( toDto( prestataire ) );
        }

        return list;
    }

    @Override
    public Prestataire toEntity(PrestataireDto dto) throws ParseException {
        if ( dto == null ) {
            return null;
        }

        Prestataire prestataire = new Prestataire();

        prestataire.setCodePrestataire( dto.getCodePrestataire() );
        if ( dto.getCreatedAt() != null ) {
            prestataire.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getCreatedAt() ) );
        }
        prestataire.setUpdatedBy( dto.getUpdatedBy() );
        prestataire.setIsDeleted( dto.getIsDeleted() );
        prestataire.setCreatedBy( dto.getCreatedBy() );
        prestataire.setContact( dto.getContact() );
        prestataire.setId( dto.getId() );
        prestataire.setNom( dto.getNom() );
        prestataire.setPrenom( dto.getPrenom() );
        if ( dto.getUpdatedAt() != null ) {
            prestataire.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getUpdatedAt() ) );
        }

        return prestataire;
    }
}
