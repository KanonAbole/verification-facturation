package com.softech.facturation.app.utils.dto.transformer;

import com.softech.facturation.app.dao.entity.NatureActe;
import com.softech.facturation.app.utils.dto.NatureActeDto;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-15T12:06:42+0000",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_352 (Private Build)"
)
public class NatureActeTransformerImpl implements NatureActeTransformer {

    @Override
    public NatureActeDto toDto(NatureActe entity) throws ParseException {
        if ( entity == null ) {
            return null;
        }

        NatureActeDto natureActeDto = new NatureActeDto();

        if ( entity.getCreatedAt() != null ) {
            natureActeDto.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getCreatedAt() ) );
        }
        if ( entity.getUpdatedAt() != null ) {
            natureActeDto.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getUpdatedAt() ) );
        }
        natureActeDto.setId( entity.getId() );
        natureActeDto.setLibelle( entity.getLibelle() );
        natureActeDto.setCreatedBy( entity.getCreatedBy() );
        natureActeDto.setUpdatedBy( entity.getUpdatedBy() );
        natureActeDto.setIsDeleted( entity.getIsDeleted() );

        return natureActeDto;
    }

    @Override
    public List<NatureActeDto> toDtos(List<NatureActe> entities) throws ParseException {
        if ( entities == null ) {
            return null;
        }

        List<NatureActeDto> list = new ArrayList<NatureActeDto>( entities.size() );
        for ( NatureActe natureActe : entities ) {
            list.add( toDto( natureActe ) );
        }

        return list;
    }

    @Override
    public NatureActe toEntity(NatureActeDto dto) throws ParseException {
        if ( dto == null ) {
            return null;
        }

        NatureActe natureActe = new NatureActe();

        if ( dto.getCreatedAt() != null ) {
            natureActe.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getCreatedAt() ) );
        }
        natureActe.setUpdatedBy( dto.getUpdatedBy() );
        natureActe.setIsDeleted( dto.getIsDeleted() );
        natureActe.setCreatedBy( dto.getCreatedBy() );
        natureActe.setLibelle( dto.getLibelle() );
        natureActe.setId( dto.getId() );
        if ( dto.getUpdatedAt() != null ) {
            natureActe.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getUpdatedAt() ) );
        }

        return natureActe;
    }
}
