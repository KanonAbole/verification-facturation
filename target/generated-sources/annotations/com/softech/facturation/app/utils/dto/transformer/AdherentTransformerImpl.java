package com.softech.facturation.app.utils.dto.transformer;

import com.softech.facturation.app.dao.entity.Adherent;
import com.softech.facturation.app.dao.entity.Souscripteur;
import com.softech.facturation.app.utils.dto.AdherentDto;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-15T12:06:42+0000",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_352 (Private Build)"
)
public class AdherentTransformerImpl implements AdherentTransformer {

    @Override
    public AdherentDto toDto(Adherent entity) throws ParseException {
        if ( entity == null ) {
            return null;
        }

        AdherentDto adherentDto = new AdherentDto();

        if ( entity.getCreatedAt() != null ) {
            adherentDto.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getCreatedAt() ) );
        }
        if ( entity.getDateNaissance() != null ) {
            adherentDto.setDateNaissance( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getDateNaissance() ) );
        }
        String libelle = entitySouscripteurLibelle( entity );
        if ( libelle != null ) {
            adherentDto.setSouscripteurLibelle( libelle );
        }
        Integer id = entitySouscripteurId( entity );
        if ( id != null ) {
            adherentDto.setIdSouscripteur( id );
        }
        if ( entity.getUpdatedAt() != null ) {
            adherentDto.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getUpdatedAt() ) );
        }
        adherentDto.setId( entity.getId() );
        adherentDto.setMatricule( entity.getMatricule() );
        adherentDto.setNumAdherent( entity.getNumAdherent() );
        adherentDto.setTauxPriseCharge( entity.getTauxPriseCharge() );
        adherentDto.setNom( entity.getNom() );
        adherentDto.setPrenom( entity.getPrenom() );
        adherentDto.setLieuNaissance( entity.getLieuNaissance() );
        adherentDto.setAgent( entity.getAgent() );
        adherentDto.setFonction( entity.getFonction() );
        adherentDto.setTelephone1( entity.getTelephone1() );
        adherentDto.setTelephone2( entity.getTelephone2() );
        adherentDto.setSexe( entity.getSexe() );
        adherentDto.setPhoto( entity.getPhoto() );
        adherentDto.setCreatedBy( entity.getCreatedBy() );
        adherentDto.setUpdatedBy( entity.getUpdatedBy() );
        adherentDto.setIsDeleted( entity.getIsDeleted() );

        return adherentDto;
    }

    @Override
    public List<AdherentDto> toDtos(List<Adherent> entities) throws ParseException {
        if ( entities == null ) {
            return null;
        }

        List<AdherentDto> list = new ArrayList<AdherentDto>( entities.size() );
        for ( Adherent adherent : entities ) {
            list.add( toDto( adherent ) );
        }

        return list;
    }

    @Override
    public Adherent toEntity(AdherentDto dto, Souscripteur souscripteur) throws ParseException {
        if ( dto == null && souscripteur == null ) {
            return null;
        }

        Adherent adherent = new Adherent();

        if ( dto != null ) {
            adherent.setAgent( dto.getAgent() );
            adherent.setUpdatedBy( dto.getUpdatedBy() );
            adherent.setMatricule( dto.getMatricule() );
            if ( dto.getDateNaissance() != null ) {
                adherent.setDateNaissance( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getDateNaissance() ) );
            }
            adherent.setTelephone2( dto.getTelephone2() );
            adherent.setPhoto( dto.getPhoto() );
            adherent.setTelephone1( dto.getTelephone1() );
            adherent.setSexe( dto.getSexe() );
            adherent.setNom( dto.getNom() );
            adherent.setTauxPriseCharge( dto.getTauxPriseCharge() );
            adherent.setNumAdherent( dto.getNumAdherent() );
            adherent.setLieuNaissance( dto.getLieuNaissance() );
            if ( dto.getCreatedAt() != null ) {
                adherent.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getCreatedAt() ) );
            }
            adherent.setIsDeleted( dto.getIsDeleted() );
            adherent.setCreatedBy( dto.getCreatedBy() );
            adherent.setFonction( dto.getFonction() );
            adherent.setId( dto.getId() );
            adherent.setPrenom( dto.getPrenom() );
            if ( dto.getUpdatedAt() != null ) {
                adherent.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getUpdatedAt() ) );
            }
        }
        if ( souscripteur != null ) {
            adherent.setSouscripteur( souscripteur );
        }

        return adherent;
    }

    private String entitySouscripteurLibelle(Adherent adherent) {
        if ( adherent == null ) {
            return null;
        }
        Souscripteur souscripteur = adherent.getSouscripteur();
        if ( souscripteur == null ) {
            return null;
        }
        String libelle = souscripteur.getLibelle();
        if ( libelle == null ) {
            return null;
        }
        return libelle;
    }

    private Integer entitySouscripteurId(Adherent adherent) {
        if ( adherent == null ) {
            return null;
        }
        Souscripteur souscripteur = adherent.getSouscripteur();
        if ( souscripteur == null ) {
            return null;
        }
        Integer id = souscripteur.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
