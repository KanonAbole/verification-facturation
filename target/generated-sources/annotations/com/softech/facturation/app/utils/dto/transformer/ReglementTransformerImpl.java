package com.softech.facturation.app.utils.dto.transformer;

import com.softech.facturation.app.dao.entity.Facture;
import com.softech.facturation.app.dao.entity.Reglement;
import com.softech.facturation.app.utils.dto.ReglementDto;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-15T12:06:42+0000",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_352 (Private Build)"
)
public class ReglementTransformerImpl implements ReglementTransformer {

    @Override
    public ReglementDto toDto(Reglement entity) throws ParseException {
        if ( entity == null ) {
            return null;
        }

        ReglementDto reglementDto = new ReglementDto();

        if ( entity.getCreatedAt() != null ) {
            reglementDto.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getCreatedAt() ) );
        }
        Integer id = entityFactureId( entity );
        if ( id != null ) {
            reglementDto.setIdFacture( id );
        }
        if ( entity.getDateReglement() != null ) {
            reglementDto.setDateReglement( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getDateReglement() ) );
        }
        if ( entity.getUpdatedAt() != null ) {
            reglementDto.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getUpdatedAt() ) );
        }
        reglementDto.setId( entity.getId() );
        reglementDto.setNumReglement( entity.getNumReglement() );
        reglementDto.setMontantRegle( entity.getMontantRegle() );
        reglementDto.setMontantLettre( entity.getMontantLettre() );
        reglementDto.setModeReglement( entity.getModeReglement() );
        reglementDto.setNumCheque( entity.getNumCheque() );
        reglementDto.setCreatedBy( entity.getCreatedBy() );
        reglementDto.setUpdatedBy( entity.getUpdatedBy() );
        reglementDto.setIsDeleted( entity.getIsDeleted() );

        return reglementDto;
    }

    @Override
    public List<ReglementDto> toDtos(List<Reglement> entities) throws ParseException {
        if ( entities == null ) {
            return null;
        }

        List<ReglementDto> list = new ArrayList<ReglementDto>( entities.size() );
        for ( Reglement reglement : entities ) {
            list.add( toDto( reglement ) );
        }

        return list;
    }

    @Override
    public Reglement toEntity(ReglementDto dto, Facture facture) throws ParseException {
        if ( dto == null && facture == null ) {
            return null;
        }

        Reglement reglement = new Reglement();

        if ( dto != null ) {
            reglement.setUpdatedBy( dto.getUpdatedBy() );
            reglement.setMontantRegle( dto.getMontantRegle() );
            reglement.setNumCheque( dto.getNumCheque() );
            if ( dto.getCreatedAt() != null ) {
                reglement.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getCreatedAt() ) );
            }
            reglement.setNumReglement( dto.getNumReglement() );
            reglement.setIsDeleted( dto.getIsDeleted() );
            reglement.setCreatedBy( dto.getCreatedBy() );
            if ( dto.getDateReglement() != null ) {
                reglement.setDateReglement( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getDateReglement() ) );
            }
            reglement.setModeReglement( dto.getModeReglement() );
            reglement.setId( dto.getId() );
            reglement.setMontantLettre( dto.getMontantLettre() );
            if ( dto.getUpdatedAt() != null ) {
                reglement.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getUpdatedAt() ) );
            }
        }
        if ( facture != null ) {
            reglement.setFacture( facture );
        }

        return reglement;
    }

    private Integer entityFactureId(Reglement reglement) {
        if ( reglement == null ) {
            return null;
        }
        Facture facture = reglement.getFacture();
        if ( facture == null ) {
            return null;
        }
        Integer id = facture.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
