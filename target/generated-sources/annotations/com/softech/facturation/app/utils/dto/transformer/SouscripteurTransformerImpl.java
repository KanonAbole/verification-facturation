package com.softech.facturation.app.utils.dto.transformer;

import com.softech.facturation.app.dao.entity.Souscripteur;
import com.softech.facturation.app.utils.dto.SouscripteurDto;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-15T12:06:42+0000",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_352 (Private Build)"
)
public class SouscripteurTransformerImpl implements SouscripteurTransformer {

    @Override
    public SouscripteurDto toDto(Souscripteur entity) throws ParseException {
        if ( entity == null ) {
            return null;
        }

        SouscripteurDto souscripteurDto = new SouscripteurDto();

        if ( entity.getCreatedAt() != null ) {
            souscripteurDto.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getCreatedAt() ) );
        }
        if ( entity.getUpdatedAt() != null ) {
            souscripteurDto.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getUpdatedAt() ) );
        }
        souscripteurDto.setId( entity.getId() );
        souscripteurDto.setLibelle( entity.getLibelle() );
        souscripteurDto.setCreatedBy( entity.getCreatedBy() );
        souscripteurDto.setUpdatedBy( entity.getUpdatedBy() );
        souscripteurDto.setIsDeleted( entity.getIsDeleted() );

        return souscripteurDto;
    }

    @Override
    public List<SouscripteurDto> toDtos(List<Souscripteur> entities) throws ParseException {
        if ( entities == null ) {
            return null;
        }

        List<SouscripteurDto> list = new ArrayList<SouscripteurDto>( entities.size() );
        for ( Souscripteur souscripteur : entities ) {
            list.add( toDto( souscripteur ) );
        }

        return list;
    }

    @Override
    public Souscripteur toEntity(SouscripteurDto dto) throws ParseException {
        if ( dto == null ) {
            return null;
        }

        Souscripteur souscripteur = new Souscripteur();

        if ( dto.getCreatedAt() != null ) {
            souscripteur.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getCreatedAt() ) );
        }
        souscripteur.setUpdatedBy( dto.getUpdatedBy() );
        souscripteur.setIsDeleted( dto.getIsDeleted() );
        souscripteur.setCreatedBy( dto.getCreatedBy() );
        souscripteur.setLibelle( dto.getLibelle() );
        souscripteur.setId( dto.getId() );
        if ( dto.getUpdatedAt() != null ) {
            souscripteur.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getUpdatedAt() ) );
        }

        return souscripteur;
    }
}
