package com.softech.facturation.app.utils.dto.transformer;

import com.softech.facturation.app.dao.entity.Adherent;
import com.softech.facturation.app.dao.entity.Facture;
import com.softech.facturation.app.dao.entity.NatureActe;
import com.softech.facturation.app.dao.entity.Prestataire;
import com.softech.facturation.app.utils.dto.FactureDto;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-15T12:06:42+0000",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_352 (Private Build)"
)
public class FactureTransformerImpl implements FactureTransformer {

    @Override
    public FactureDto toDto(Facture entity) throws ParseException {
        if ( entity == null ) {
            return null;
        }

        FactureDto factureDto = new FactureDto();

        if ( entity.getCreatedAt() != null ) {
            factureDto.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getCreatedAt() ) );
        }
        if ( entity.getDateSoin() != null ) {
            factureDto.setDateSoin( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getDateSoin() ) );
        }
        Integer id = entityAdherentId( entity );
        if ( id != null ) {
            factureDto.setIdAdherent( id );
        }
        Integer id1 = entityPrestataireId( entity );
        if ( id1 != null ) {
            factureDto.setIdPrestataire( id1 );
        }
        String prenom = entityPrestatairePrenom( entity );
        if ( prenom != null ) {
            factureDto.setPrestatairePrenom( prenom );
        }
        String nom = entityPrestataireNom( entity );
        if ( nom != null ) {
            factureDto.setPrestataireNom( nom );
        }
        Integer id2 = entityNatureActeId( entity );
        if ( id2 != null ) {
            factureDto.setIdNatureActe( id2 );
        }
        String libelle = entityNatureActeLibelle( entity );
        if ( libelle != null ) {
            factureDto.setNatureActeLibelle( libelle );
        }
        String nom1 = entityAdherentNom( entity );
        if ( nom1 != null ) {
            factureDto.setAdherentNom( nom1 );
        }
        String prenom1 = entityAdherentPrenom( entity );
        if ( prenom1 != null ) {
            factureDto.setAdherentPrenom( prenom1 );
        }
        if ( entity.getUpdatedAt() != null ) {
            factureDto.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).format( entity.getUpdatedAt() ) );
        }
        factureDto.setId( entity.getId() );
        factureDto.setNumFacture( entity.getNumFacture() );
        factureDto.setNumBon( entity.getNumBon() );
        factureDto.setMontantTtc( entity.getMontantTtc() );
        factureDto.setTauxPcFacture( entity.getTauxPcFacture() );
        factureDto.setMontantExclu( entity.getMontantExclu() );
        factureDto.setMontantCharge( entity.getMontantCharge() );
        factureDto.setPartPatient( entity.getPartPatient() );
        factureDto.setCodeAffection( entity.getCodeAffection() );
        factureDto.setNumTransmission( entity.getNumTransmission() );
        factureDto.setPartAssurance( entity.getPartAssurance() );
        factureDto.setCreatedBy( entity.getCreatedBy() );
        factureDto.setUpdatedBy( entity.getUpdatedBy() );
        factureDto.setCodeMedPrescripteur( entity.getCodeMedPrescripteur() );
        factureDto.setEstTraiter( entity.getEstTraiter() );
        factureDto.setEstPayer( entity.getEstPayer() );
        factureDto.setIsDeleted( entity.getIsDeleted() );

        return factureDto;
    }

    @Override
    public List<FactureDto> toDtos(List<Facture> entities) throws ParseException {
        if ( entities == null ) {
            return null;
        }

        List<FactureDto> list = new ArrayList<FactureDto>( entities.size() );
        for ( Facture facture : entities ) {
            list.add( toDto( facture ) );
        }

        return list;
    }

    @Override
    public Facture toEntity(FactureDto dto, NatureActe natureActe, Adherent adherent, Prestataire prestataire) throws ParseException {
        if ( dto == null && natureActe == null && adherent == null && prestataire == null ) {
            return null;
        }

        Facture facture = new Facture();

        if ( dto != null ) {
            facture.setPartPatient( dto.getPartPatient() );
            facture.setNumBon( dto.getNumBon() );
            facture.setUpdatedBy( dto.getUpdatedBy() );
            facture.setTauxPcFacture( dto.getTauxPcFacture() );
            facture.setEstPayer( dto.getEstPayer() );
            facture.setCodeMedPrescripteur( dto.getCodeMedPrescripteur() );
            facture.setMontantExclu( dto.getMontantExclu() );
            facture.setMontantCharge( dto.getMontantCharge() );
            facture.setEstTraiter( dto.getEstTraiter() );
            facture.setPartAssurance( dto.getPartAssurance() );
            facture.setMontantTtc( dto.getMontantTtc() );
            facture.setNumFacture( dto.getNumFacture() );
            if ( dto.getCreatedAt() != null ) {
                facture.setCreatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getCreatedAt() ) );
            }
            facture.setIsDeleted( dto.getIsDeleted() );
            if ( dto.getDateSoin() != null ) {
                facture.setDateSoin( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getDateSoin() ) );
            }
            facture.setCreatedBy( dto.getCreatedBy() );
            facture.setId( dto.getId() );
            facture.setNumTransmission( dto.getNumTransmission() );
            facture.setCodeAffection( dto.getCodeAffection() );
            if ( dto.getUpdatedAt() != null ) {
                facture.setUpdatedAt( new SimpleDateFormat( "dd/MM/yyyy" ).parse( dto.getUpdatedAt() ) );
            }
        }
        if ( natureActe != null ) {
            facture.setNatureActe( natureActe );
        }
        if ( adherent != null ) {
            facture.setAdherent( adherent );
        }
        if ( prestataire != null ) {
            facture.setPrestataire( prestataire );
        }

        return facture;
    }

    private Integer entityAdherentId(Facture facture) {
        if ( facture == null ) {
            return null;
        }
        Adherent adherent = facture.getAdherent();
        if ( adherent == null ) {
            return null;
        }
        Integer id = adherent.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Integer entityPrestataireId(Facture facture) {
        if ( facture == null ) {
            return null;
        }
        Prestataire prestataire = facture.getPrestataire();
        if ( prestataire == null ) {
            return null;
        }
        Integer id = prestataire.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private String entityPrestatairePrenom(Facture facture) {
        if ( facture == null ) {
            return null;
        }
        Prestataire prestataire = facture.getPrestataire();
        if ( prestataire == null ) {
            return null;
        }
        String prenom = prestataire.getPrenom();
        if ( prenom == null ) {
            return null;
        }
        return prenom;
    }

    private String entityPrestataireNom(Facture facture) {
        if ( facture == null ) {
            return null;
        }
        Prestataire prestataire = facture.getPrestataire();
        if ( prestataire == null ) {
            return null;
        }
        String nom = prestataire.getNom();
        if ( nom == null ) {
            return null;
        }
        return nom;
    }

    private Integer entityNatureActeId(Facture facture) {
        if ( facture == null ) {
            return null;
        }
        NatureActe natureActe = facture.getNatureActe();
        if ( natureActe == null ) {
            return null;
        }
        Integer id = natureActe.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private String entityNatureActeLibelle(Facture facture) {
        if ( facture == null ) {
            return null;
        }
        NatureActe natureActe = facture.getNatureActe();
        if ( natureActe == null ) {
            return null;
        }
        String libelle = natureActe.getLibelle();
        if ( libelle == null ) {
            return null;
        }
        return libelle;
    }

    private String entityAdherentNom(Facture facture) {
        if ( facture == null ) {
            return null;
        }
        Adherent adherent = facture.getAdherent();
        if ( adherent == null ) {
            return null;
        }
        String nom = adherent.getNom();
        if ( nom == null ) {
            return null;
        }
        return nom;
    }

    private String entityAdherentPrenom(Facture facture) {
        if ( facture == null ) {
            return null;
        }
        Adherent adherent = facture.getAdherent();
        if ( adherent == null ) {
            return null;
        }
        String prenom = adherent.getPrenom();
        if ( prenom == null ) {
            return null;
        }
        return prenom;
    }
}
